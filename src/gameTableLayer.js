var RLTGameTableLayer = cc.Layer.extend({
	table: null,
	model: null,
	ctor: function(delegate, model)
	{
		this._super();		
		this.model = model;
		
		// игровой стол
		this.table = new RLTTable(cc.plistParser.parse(jsb.fileUtils.getStringFromFile(res.LightZone_plistDescription)), 
									delegate, model);
		this.table.enable = false;
		//this.table.setAnchorPoint(cc.p(0.0, 0.0));
		var size = cc.director.getWinSize();
	//	this.table.setPosition(cc.p(size.width/2, size.height/2));
		this.table.contentSize = size;
		this.table.wheel.setDelegate(this);		
		this.addChild(this.table);
		
		var sprite = cc.createColorSprite(cc.size(100, 100), cc.color(255, 0, 0, 0));
		this.addChild(sprite);
				
	},
	update: function(time)
	{
		this._super(time);
		this.table.update(time);
	},
	enable: {
		writable: true,
		value: false,
		set: function(_enable) {
			
		}
	}
});
//----------------------------------------------------------------------------------------------------------------
RLTGameTableLayer.prototype.showWin = function() {

	this.table.enable = false;
	this.table.startFlyChips();
	this.model.DidEndShowWin();
}
//----------------------------------------------------------------------------------------------------------------
RLTGameTableLayer.prototype.placeYourBets = function() {
	this.table.wheel.reset();
	
	this.table.selectedChipPanel.enable = true;
	this.table.updateSelectedChips();
	this.table.enable = true;
}
//----------------------------------------------------------------------------------------------------------------
RLTGameTableLayer.prototype.noMoreBets = function() {
	this.table.wheel.startOrbita();
	this.table.enable = false;
}
//----------------------------------------------------------------------------------------------------------------
RLTGameTableLayer.prototype.startFallToHole = function() {
	// устанавливаем номер, шарик на котором должно остановиться колесо рулетки
	this.table.wheel.setStopNumber(this.model.GetWinNumber());
}
//----------------------------------------------------------------------------------------------------------------
RLTGameTableLayer.prototype.clearAllChipsOnTable = function() {

}
//----------------------------------------------------------------------------------------------------------------
RLTGameTableLayer.prototype.modelDidInit = function() {
	this.table.wheel.start();
	
	this.table.selectedChipPanel.enable = true;
	this.table.updateSelectedChips();
	this.table.enable = this.model.getCurrentState == RLTMODELSTATE.STATE_MAKE_YOUR_BETS;
	if(this.model.GetCredits() > 0)
	{
		this.table.changeBet(0);
	}
}
//----------------------------------------------------------------------------------------------------------------
RLTGameTableLayer.prototype.didFallInHole = function(){
	//[_zoomWheel showWheelWithNumber:[_model winNumber]];
	this.model.DidFallInHole();
}
//----------------------------------------------------------------------------------------------------------------
RLTGameTableLayer.prototype.didMakeBet = function(field, value) 
{
	return this.table.didMakeBet(field,value);
}
//----------------------------------------------------------------------------------------------------------------
RLTGameTableLayer.prototype.didTakeBetSuccess = function() 
{
	cc.log("didTakeBetSuccess");
	this.table.updateSelectedChips();
}


