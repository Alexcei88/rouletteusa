/*Object.defineProperty(Object.prototype,'Enum', {
	value: function() {
		for(i in arguments) {
			Object.defineProperty(this,arguments[i], {
				value:parseInt(i),
				writable:false,
				enumerable:true,
				configurable:true
			});
		}
		return this;
	},
	writable:false,
	enumerable:false,
	configurable:false
});*/ 

var RLTPARAMCALC = {};
RLTPARAMCALC.Enum(
		'RLT_NOPARAMETER',    /**< Не расчитываь автоматический параметр (значение по-умолчанию) */
		'RLT_START',          /**< Расчет начального значения вычисляемой величины */
		'RLT_END',            /**< Расчет конечного значения вычисляемой величины */
		'RLT_SPEED',          /**< Расчет скорости изменения значения вычисляемой величины */
		'RLT_ACCELERATION'    /**< Расчет ускорения изменения значения вычисляемой величины */		
		);

function RLTLawMoveWheelElement(start
		, end
		, speed
		, accel
		, time
		, param
)
{
	var start = start;
	var end   = end; 
	var speed = speed;
	var accel = accel;
	var time  = time;
	
	switch(param)
	{
		case RLTPARAMCALC.RLT_NOPARAMETER: break;
		case RLTPARAMCALC.RLT_START: 
			start  = end - (speed * time) - ((accel * time * time) / 2.0);
			break;
			
		case RLTPARAMCALC.RLT_END: 
			end  = start + (speed * time) + ((accel * time * time) / 2.0);
			break;
		case RLTPARAMCALC.RLT_SPEED: 
			speed  = ((end - start) / time) - (accel * time / 2.0);
			break;
		case RLTPARAMCALC.RLT_ACCELERATION: 
			accel  = (2.0 * ((end - start) - speed * time)) / (time * time);
			break;
			
	}
	
	var elapsedTime = 0;
	var previousValue = start;

	this.isFinished = false;
	
	this.update = function(time) {
		
		if(this.isFinished)
			return;

		var currentTime = elapsedTime + time;
		var currentValue = start + speed * currentTime + accel * currentTime * currentTime / 2.0;

		var isReached = false;

		// проверяем - не перевалили ли через конечное значение
		if((end - start) >= 0.0)
		{
			if(currentValue >= end){
				isReached = true;
			}
			// иначе проверяем не изменился ли знак скорости (если использованы параметры как в законе Decelerated)
			else {
				if((currentValue - previousValue) < 0.0)
				{
					isReached = true;
				}
			}
		}
		// проверяем - не перевалили ли через конечное значение
		else if((end - start) <= 0.0)
		{
			if(currentValue <= end) {
				isReached = true;
			}
			// иначе проверяем не изменился ли знак скорости (если использованы параметры как в законе Decelerated)
			else {
				if((currentValue - previousValue) > 0.0)
				{
					isReached = true;
				}
			}
		}

		if(isReached)
		{
			this.value = end;
			this.isFinished = true;
			previousValue = start;
		}
		else
		{
			this.value = currentValue;
			elapsedTime += time;
			previousValue = currentValue;
		}				
	}
}

RLTLawMoveWheelElement.prototype.isFinished = false;
RLTLawMoveWheelElement.prototype.value = -1;
