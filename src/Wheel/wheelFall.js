var RLTWheelFall = RLTWheelElement.extend({
	ctor: function(spriteFrameName, frameCount, delayDefault, startFrame, posOrbita,
			posDeltaWheel)
	{
		this._super(spriteFrameName, frameCount, delayDefault, startFrame);
		
		this.delta = posDeltaWheel;
		this.frameOrbita = posOrbita;
		//список номеров, на которые будет выпадать сходящий шарик
		var number = [28, 0, 2, 14, 35, 23, 4, 16, 33, 21, 6, 18, 31, 19, 8
		              ,12, 29, 25, 10, 27, 37, 1, 13, 36, 24, 3, 15, 34, 22
		              ,5, 17, 32, 20, 7, 11, 30, 26, 9];
		
		this.getNumber = function() {
			return number;
		}
	},
	//смещение колеса относительно 0, откуда начнется анимация схода шарика
	delta: 0,
	frameOrbita: 0,
	
	updateAnimation: function(time) {
		
		var currentTime = this._elapsedTime + time;
		if(currentTime < this.delay)
		{
			this._elapsedTime += time;
			return;
		}
		while(currentTime >= this.delay)
		{
			currentTime -= this.delay;			
			this.nextFrame();
			if(this.currentFrame == (this.frameCount - 1))
				break;
		}
		this._elapsedTime = currentTime;
	}
})
//-------------------------------------------------------------------------------------------------------------
RLTWheelFall.prototype.getFrameWheelWithNumber = function(number)
{
	var index = this.getNumber().indexOf(number, 0);
	if(index < this.delta)
		index = 38 - (this.delta - index);
	else
		index -= this.delta;
	return index * 10;
};
//-------------------------------------------------------------------------------------------------------------
RLTWheelFall.prototype.getNumberWheelFrames = function(number, curFrameWheel) {
	// какой кадр должен быть у колеса, чтобы шарик сошел на нужный номер:
	var needFrame = this.getFrameWheelWithNumber(number);

	// сколько кадров нужно отщелкать от текущего до нужного
	if(needFrame <= curFrameWheel)
		return 380 - curFrameWheel + needFrame;
	else
		return needFrame - curFrameWheel;
};
//-------------------------------------------------------------------------------------------------------------
RLTWheelFall.prototype.getNumberOrbitaFrames = function(curFrameOrbita)
{
	// какой кадр должен быть у орбиты в тот момент, когда она окажется на точке схода:
	var needFrame = this.frameOrbita;

	//сколько кадров нужно отщелкать от текущего до нужного
	if(needFrame <= curFrameOrbita)
		return 190 - curFrameOrbita + needFrame;
	else
		return needFrame - curFrameOrbita;
};
//-------------------------------------------------------------------------------------------------------------
RLTWheelFall.prototype.isSignalFall = function(number, frameWheel, frameOrbita)
{
	var successW = this.getFrameWheelWithNumber(number) == frameWheel;
	var successO = this.frameOrbita == frameOrbita;
	return successO && successW;
};