//время переключение между кадрами элементов колеса
var kSwitchDelayWheelElement = 0.012;
//максимально допустимое значения отклонения для шарика
var kMaxDeviationOrbita = 2.1;

var kFrameCountOrbita = 190;
var kFrameCountHole = 380;
//количество проработанных сходов шарика
var kNumberFalls = 10;

var RLTEnumSTATE = {};
RLTEnumSTATE.Enum(
		'STATE_HIDE'          ,    //колесо не вращается, и не отображается
		'STATE_INFINITY'      ,    //бесконечное вращение колеса без шарика
		'STATE_ORBITA'        ,    //бесконечное вращение на орбите, номер остановки не известен
		'STATE_CALCULATION'   ,    //расчет алгоритма, как подвинуть орбиту и колесо к точке схода
		'STATE_ORBITAFALL'    ,    //номер остановки известен,шарик на орбите движется к точке схода
		'STATE_FALL'          ,    //шарик сходит с орбиты в лунку
		'STATE_BEGHOLE'       ,    //подготовка анимации шарика в лунке
		'STATE_HOLE'          ,    //шарик покоится в лунке
		'STATE_STOP'               //полная остановка колеса и шарика
);

var RLTWheel = cc.Node.extend({
	state: null,
	wheel: null,
	orbita: null,
	hole: null,
	falls: null,
	currentIndexFall: -1,
	stopNumber: 0,
	// номер кадра орбиты, на котором было вычисление
	frameOrbitaCalculation: 0,
	ctor: function() 
	{
		this._super();

		this.delegate = null;
		function loadWheel() {
			cc.spriteFrameCache.addSpriteFrames(res.Wheel_part1_sheet_plist, res.Wheel_part1_sheet_png);
			cc.spriteFrameCache.addSpriteFrames(res.Wheel_part2_sheet_plist, res.Wheel_part2_sheet_png);
			this.wheel = new RLTWheelElement("wheel_{0}.png", 380, kSwitchDelayWheelElement, 0);
			this.wheel.setPosition(cc.p(48, 735));
			this.wheel.setAnchorPoint(cc.p(0, 1));
			this.addChild(this.wheel);

			//  смена анимации шарика кручения по орбите на анимацию схода шарика
			this.changeOrbitaWithFall = false;
			
			function addFrame(name, position) {
				var sprite = cc.Sprite.createWithSpriteFrameName(name);
				sprite.setAnchorPoint(cc.p(0, 1));
				sprite.setPosition(position);
				this.addChild(sprite);				
			}

			addFrame.call(this, "wheel_left.png", cc.p(0, 735));
			addFrame.call(this, "wheel_right.png", cc.p(321, 735));
			addFrame.call(this, "wheel_top.png", cc.p(48, 769));
			addFrame.call(this, "wheel_bottom.png", cc.p(48, 582));
		};		
		
		// загрузка колеса рулетки
		loadWheel.call(this);

		function loadOrbitaAndHole() {
			cc.spriteFrameCache.addSpriteFrames(res.Wheel_orbita_and_hole_plist);

			// загрузка орбиты
			this.orbita = new RLTWheelElement("orbita_{0}.png", kFrameCountOrbita, kSwitchDelayWheelElement, 0);
			this.orbita.setAnchorPoint(cc.p(0, 1));
			this.orbita.setPosition(cc.p(0, 769));
			this.orbita.setVisible(false);
			this.addChild(this.orbita);
	
			// загрузка падания шарика
			this.hole = new RLTWheelElement("hole_{0}.png", kFrameCountHole, kSwitchDelayWheelElement, 0);
			this.hole.setAnchorPoint(cc.p(0, 1));
			this.hole.setPosition(cc.p(0, 769));
			this.hole.setVisible(false);
			this.addChild(this.hole);
		}
		
		loadOrbitaAndHole.call(this);
	
		function loadFalls() 
		{
			cc.spriteFrameCache.addSpriteFrames(res.Wheel_fall0_plist);
			cc.spriteFrameCache.addSpriteFrames(res.Wheel_fall1_plist);
			cc.spriteFrameCache.addSpriteFrames(res.Wheel_fall2_plist);
			cc.spriteFrameCache.addSpriteFrames(res.Wheel_fall3_plist);
			cc.spriteFrameCache.addSpriteFrames(res.Wheel_fall4_plist);
			cc.spriteFrameCache.addSpriteFrames(res.Wheel_fall5_plist);
			cc.spriteFrameCache.addSpriteFrames(res.Wheel_fall6_plist);
			cc.spriteFrameCache.addSpriteFrames(res.Wheel_fall7_plist);
			cc.spriteFrameCache.addSpriteFrames(res.Wheel_fall8_plist);
			cc.spriteFrameCache.addSpriteFrames(res.Wheel_fall9_plist);

			this.falls = [];
			var doc = cc.plistParser.parse(jsb.fileUtils.getStringFromFile(res.Falls_description_plist));
			var Falls = doc["Falls"];
			var frameCount = Falls["Common"]["frame_count"]
			
			var _falls = Falls["Common"]["falls"];
			for(var i = 0; i < _falls.length; ++i)
			{
				var fallItem = _falls[i];
				var fall = new RLTWheelFall(fallItem["sprite_frame_name"],frameCount,
						kSwitchDelayWheelElement,
						0,
						fallItem["frame_falls_orbita"],
						fallItem["frame_wheel_deta"]
						);
				fall.setAnchorPoint(cc.p(0, 1));				
				fall.setPosition(cc.p(0, 769));
				fall.setVisible(false);
				this.addChild(fall);
				this.falls.push(fall);
			}
		}
				
		loadFalls.call(this);
		
		this.numbersWheelInHole = [28, 9, 26, 30, 11, 7, 20, 32, 17, 5, 22, 34, 15
		                        ,3, 24, 36, 13, 1, 37, 27, 10, 25, 29, 12, 8, 19
		                        ,31, 18, 6, 21,33, 16, 4, 23, 35, 14, 2, 0];
		
		// инициализируем состояние
		this.state = RLTEnumSTATE.STATE_HIDE;
	},
	update: function(time){
		this._super(time);
		
		
		switch(this.state)
		{
		case RLTEnumSTATE.STATE_ACCELERATION:
		{
			this.wheel.updateAnimation(time);
			return;
		}

		case RLTEnumSTATE.STATE_INFINITY:
		{
			this.wheel.updateAnimation(time);
			//[_rollLoop stop];
			return;
		}
		case RLTEnumSTATE.STATE_ORBITA:
		{
			this.wheel.updateAnimation(time);
			this.orbita.updateAnimation(time);
			//[self checkMusicPlayingWhileOrbitaRound];
			return;
		}
		case RLTEnumSTATE.STATE_CALCULATION:
		{
			this.wheel.updateAnimation(time);
			this.orbita.updateAnimation(time);
			this.playCalculation(time);
			//[self checkMusicPlayingWhileOrbitaRound];
			return;
		}
		
		case RLTEnumSTATE.STATE_ORBITAFALL:
		{
			this.wheel.updateAnimation(time);
			this.orbita.updateAnimation(time);
			//[self checkMusicPlayingWhileOrbitaRound];
			if(this.orbita.InSignalFrame())
			{
				var currentFall = this.falls[this.currentIndexFall];
				currentFall.currentFrame = 0;
				currentFall.reStartDelay();
				currentFall.setVisible(this.orbita.skipFrame == 0);

				this.wheel.reStartDelay();
				this.orbita.setVisible(this.orbita.skipFrame != 0);
				this.changeOrbitaWithFall = this.orbita.skipFrame == 0;
				if(this.orbita.skipFrame != 0)
				{
					currentFall.skipFrame = this.orbita.skipFrame;
					// включаем режим ожидания сигнального кадра
					this.orbita.modeWaitOnSignalFrame = true;
					this.orbita.countFrameInModeWaitSignalFrame = this.orbita.skipFrame;
					cc.log("Включаем режим ожидания сигнального кадра у орбиты");
				}

				cc.log("state = STATE_FALL");
				this.state = RLTEnumSTATE.STATE_FALL;
			}
			else
			{
				function Recalculation() {
					if(this.orbita.currentFrame > this.orbita.signalFrame)
					{
						cc.log("RECALCULATION: state = STATE_CALCULATION");
						this.state = RLTEnumSTATE.STATE_CALCULATION;
					}
				};

				// если сигнальный фрейм больше фрейма, на котором идет вычисление схода шарика
				if(this.frameOrbitaCalculation < this.orbita.signalFrame)
					Recalculation.call(this);
				else
				{
					// началась ли анимация с нулевого кадра
					if(this.orbita.currentFrame < this.frameOrbitaCalculation)
						Recalculation.call(this);
				}
			}
			return;
		}
		
		case RLTEnumSTATE.STATE_FALL:
		{
			this.wheel.updateAnimation(time);
			var currentFall = this.falls[this.currentIndexFall];
			if(this.orbita.countFrameInModeWaitSignalFrame > 0)
			{
				this.orbita.updateAnimation(time);
				//[self checkMusicPlayingWhileOrbitaRound];
			}
			if(this.orbita.countFrameInModeWaitSignalFrame == 0)
			{
				if(this.changeOrbitaWithFall == false)
				{
					// выключаем режим ожидания сигнального кадра
					this.orbita.modeWaitOnSignalFrame = false;
					cc.assert(this.orbita.currentFrame == this.orbita.signalFrame, "Вышли из режима ожидания сигнального кадра, орбита должна находится в сигнальном кадре");

					this.orbita.setVisible(false);
					currentFall.setVisible(true);
					this.changeOrbitaWithFall = true;
					cc.log("change animation orbita and fall");
					currentFall.updateAnimation(this.orbita.getElapsedTime());
				}
				else
				{
					currentFall.updateAnimation(time);
					if(currentFall.currentFrame == currentFall.frameCount - 1)
					{
						this.putInLunka(this.stopNumber);
					}
					/*
					if(currentFall.currentFrame >= 108 && !_rollStop.isPlaying && !self.inLobby)
					{
						// меняем звуки
						[_rollStart stop];
						[_rollLoop stop];
						[_rollStop play];
					}*/
				}
			}

			return;
		}
		case RLTEnumSTATE.STATE_HOLE:
		{
			this.wheel.updateAnimation(time);
			this.hole.updateAnimation(time);            
			return;
		}
		case RLTEnumSTATE.STATE_DELAY:
		{
			this.wheel.updateAnimation(time);
			this.hole.updateAnimation(time);
			//[_rollLoop stop];
			return;
		}

		case RLTEnumSTATE.STATE_HIDE:
		{
			this.setVisible(false);
			return;
		}

		case RLTEnumSTATE.STATE_STOP:
		{
			this.setVisible(true);
			return;
		}

		default:
			break;
		}
	}		
});
//-------------------------------------------------------------------------------------------------------------
RLTWheel.prototype.setDelegate = function(delegate){
	this.delegate = delegate;
}
//-------------------------------------------------------------------------------------------------------------
/** \brief стартонуть колесо */
RLTWheel.prototype.start = function() {
	this.state = RLTEnumSTATE.STATE_INFINITY;
};
//-------------------------------------------------------------------------------------------------------------
/** \brief запуск орбиты */
RLTWheel.prototype.startOrbita = function() {
	if(this.state != RLTEnumSTATE.STATE_INFINITY) return;
	
	this.orbita.currentFrame = 0;
	this.orbita.reStartDelay();
	this.orbita.setVisible(true);
	
	this.hole.setVisible(false);
	for(var i = 0; i < this.falls.length; ++i)
		this.falls[i].setVisible(false);
	
	this.wheel.reStartDelay();
	this.changeOrbitaWithFall = false;
	//--------------------
	this.state = RLTEnumSTATE.STATE_ORBITA;
};
//-------------------------------------------------------------------------------------------------------------
/** \brief установить шарик, на котором должно остановится */
RLTWheel.prototype.setStopNumber = function(stopNumber) {
	if(this.state != RLTEnumSTATE.STATE_ORBITA)
		return;
	
	cc.log("setStopNumber: number = {0}".f(stopNumber));
	
	this.stopNumber = stopNumber;
	this.state = RLTEnumSTATE.STATE_CALCULATION;
};
//-------------------------------------------------------------------------------------------------------------
/** \brief сброс колеса для следующего розыгрыша */
RLTWheel.prototype.reset = function(){
	this.state = RLTEnumSTATE.STATE_INFINITY;
	
	this.orbita.setVisible(false);
	this.hole.setVisible(false);
	for(var n = 0; n < this.falls.length; ++n)
		this.falls[n].setVisible(false);

	this.wheel.reStartDelay();
};
//-------------------------------------------------------------------------------------------------------------
RLTWheel.prototype.playCalculation = function(time) 
{
	// c какой точки схода лучше сходить в лунку
	var deltaO = {value: 0};
	var coef = {value: -1};
	this.currentIndexFall = this.calculationFallOnDeltaOrbita(deltaO, coef);
	
	if(this.currentIndexFall == -1)
	{
		cc.log("no favorably falls");
		return;
	}
	
	var currentFall = this.falls[this.currentIndexFall];
	if(coef.value == 1.0)
	{
		cc.log("Идеальная ситуация, начинаем падение шарика в лунку");
		this.wheel.reStartDelay();
		currentFall.reStartDelay();
		currentFall.currentFrame = 0;
		currentFall.setVisible(false);
		
		this.orbita.setVisible(false);
		this.orbita.skipFrame = 0;
		
		self.state = RLTEnumSTATE.STATE_FALL;
		return;
	}

	cc.log("install favourites fall = {0}".f(this.currentIndexFall));
	cc.log("coeff = {0}".f(coef.value));
	cc.log("deltaO = {0}".f(deltaO.value));

	//--- установка сигнальных кадров
	this.wheel.signalFrame = currentFall.getFrameWheelWithNumber(this.stopNumber);
	this.orbita.signalFrame = currentFall.frameOrbita;

	this.moderatedDelayForElement(this.orbita, coef.value, deltaO.value);
	
	this.frameOrbitaCalculation = this.orbita.currentFrame;
	cc.log("FrameOrbitaCalculation = {0}".f(this.frameOrbitaCalculation));
	cc.log("SignalFrame = {0}".f(this.orbita.signalFrame));
	
	this.state = RLTEnumSTATE.STATE_ORBITAFALL;
}
//-------------------------------------------------------------------------------------------------------------
RLTWheel.prototype.calculationFallOnDeltaOrbita = function(deltaOrbita, coeff)
{
	var selectNumber = 0;
	var deviation = -1.0;
	while (deviation == -1.0 && selectNumber < kNumberFalls)
	{
		deviation = this.GetDeviationForNumberFall(selectNumber,deltaOrbita);
		++selectNumber;
	}
	
	if(selectNumber >= kNumberFalls)
	{
		cc.log("Не подходит ни один шарик схода рулетки");
		return -1.;
	}
	--selectNumber;
	var newDeviation;

	for(var n = selectNumber + 1; n < kNumberFalls; ++n)
	{
		newDeviation = this.GetDeviationForNumberFall(n, deltaOrbita);
		if(newDeviation == 1.0)
		{
			cc.log("100-ый сход без девиации орбиты и колеса");
			coeff.value = 1.0;
			cc.log("koeff = {0}".f(coeff.value));
			cc.log("number fall = %d", n);
			return n;
		}
		if(newDeviation == -1.0 || newDeviation > deviation)
			continue;
		deviation = newDeviation;
		selectNumber = n;
	}
	coeff.value = deviation;
	cc.log("koeff = {0}".f(coeff.value));
	cc.log("number fall = %d", selectNumber);
	return selectNumber;
}
//-------------------------------------------------------------------------------------------------------------
RLTWheel.prototype.GetDeviationForNumberFall = function(index, deltaOrbita)
{
	// сколько кадров нужно отщелкать колесу, чтобы сход с точки попал в нужну лунку
	var distW = this.falls[index].getNumberWheelFrames(this.stopNumber, this.wheel.currentFrame);
	// сколько кадров нужно отщелкать орбите, чтобы оказаться на точке схода
	var distO = this.falls[index].getNumberOrbitaFrames(this.orbita.currentFrame);
	
	deltaOrbita.value = distO;

	// колесо,и шарик уже на точке - идеальное положение
	if(distW == 0 && distO == 0)
		return 1.0;

	// либо колесо, либо шарик уже на точке (неблагоприятное положение)
	if(distW == 0 || distO==0)
		return -1.0;

	// когда ни колесо, ни шарик ещё не на точке
	if(distW != 0 && distO!=0)
	{
		var directly = distW > distO;
		if(directly == false)
			return -1.0;

		//тормозим шарик
		var coef = distW/distO;
		if(coef > kMaxDeviationOrbita)
			return -1.0;
		return coef;
	}
	return -1.0;
}
//-------------------------------------------------------------------------------------------------------------
RLTWheel.prototype.moderatedDelayForElement = function(element, coeff, delta)
{
	var end = coeff * kSwitchDelayWheelElement;
	var time = delta * end;
	var start = element.delay;
	var acceleration = 0.1;
	var speed = 0.1/kSwitchDelayWheelElement;
	
	var law = new RLTLawMoveWheelElement(start, end, speed, acceleration, time, RLTPARAMCALC.RLT_ACCELERATION);
	element.lawDelay = law;
	element.resetElapsedTime();
}
//---------------------------------------------------------------- /
RLTWheel.prototype.putInLunka = function(number)
{
	cc.log("put in lunka: number = {0}".f(number));

	var curFrameWheel = this.wheel.currentFrame;
	var index = this.numbersWheelInHole.indexOf(number, 0);
	var frame = 0;

	if(index * 10 == curFrameWheel)
		frame = curFrameWheel;
	else
	{
		frame = index * 10 + curFrameWheel;
		if(frame >= 380)
			frame = frame % 380;
	}

	cc.log("DETECTED FRAME LUNKA :{0}".f(frame));

	this.hole.currentFrame = frame;
	this.hole.setVisible(true);
	this.hole.reStartDelay();
	var fall = this.falls[this.currentIndexFall];
	fall.setVisible(false);

	this.wheel.reStartDelay();

	this.state = RLTEnumSTATE.STATE_HOLE;
	if(this.delegate)
		this.delegate.didFallInHole();
}
//-------------------------------------------------------------------------------------------------------------
