var kMaxSkipFrame = 4;

var RLTWheelElement = cc.Sprite.extend({
	// закон, по которому идет изменение задержки между кадрами
	lawDelay: null,
	// задержка между кадрами
	delay: 0,
	// сигнальный кадр 
	signalFrame: -1,
    // всего кадров 
	frameCount: 0,
/** \brief количество кадров, которые пропускаем
	 //  не больше 2 кадров
	 // если сигнальный кадр отличается от текущего меньше чем на 2, то считаем что мы в идеальном положении для схода
 */
	skipFrame: 0,
	/** \brief режим кручения элемента колеса 'ожидания сигнального кадра' */
	modeWaitOnSignalFrame: false,
	/** \brief номер проигрываемого кадра */
	currentFrame: 0,
	
	ctor: function(spriteFrameName, frameCount, delayDefault, startFrame) {
		this._super();		
		//this.initWithSpriteFrameName("wheel_0.png");
		this.frameCount = frameCount;
		this.delay = delayDefault;
		this.defaultDelay = delayDefault;
		this.currentFrame = startFrame;
		this._elapsedTime = 0.;
		this.countFrameInModeWaitSignalFrame = 0;
		this.spriteFrameName = spriteFrameName;
		
		var animation = cc.Animation.createAnimation(spriteFrameName, frameCount, delayDefault);
		cc.animationCache.addAnimation(animation, this.spriteFrameName);
		
		this._baseKoeffInterpolationSkipFrame = 0.0012;
		this._koeffInterpolationSkipFrame = this._baseKoeffInterpolationSkipFrame;
		this._interpolationSkipFrame = 0;

		this.nextFrame = function(){
			if(this.currentFrame >= (this.frameCount - 1))
				this.currentFrame = 0;
			else
				++this.currentFrame;	
			this.setDisplayFrameWithAnimationName(this.spriteFrameName, this.currentFrame);
		}		
		
		//-------------------------------------------------------------------------------------------------------------
		this.getElapsedTimeForSkipFrame = function()
		{
			if(Math.abs(this._interpolationSkipFrame) <= this._koeffInterpolationSkipFrame)
			{
				var remainder = this._interpolationSkipFrame;
				this.skipFrame = 0;
				return remainder * this.delay;
			}	
			else
			{
				this._interpolationSkipFrame -= this._koeffInterpolationSkipFrame;
				return this._koeffInterpolationSkipFrame * this.delay;
			}			
		}
	}	
});
//-------------------------------------------------------------------------------------------------------------
RLTWheelElement.prototype.updateAnimation = function(time) {
	
	if(this.lawDelay != null)
	{
		if(this.lawDelay.isFinished == false)
			this.lawDelay.update(time);
		this.delay = this.lawDelay.value;
	}
	
	var currentTime = this._elapsedTime + time + (this.skipFrame == 0 ? 0.0 : this.getElapsedTimeForSkipFrame());
	if(currentTime < this.delay)
	{
		this._elapsedTime += time;
		return;
	}
	while(currentTime >= this.delay)
	{
		currentTime -= this.delay;
		this.nextFrame();
		if(this.modeWaitOnSignalFrame)
		{
			--this.countFrameInModeWaitSignalFrame;
			if(this.countFrameInModeWaitSignalFrame <= 0)
				break;    
		}
	}
	this._elapsedTime = currentTime;
};
//-------------------------------------------------------------------------------------------------------------
RLTWheelElement.prototype.getElapsedTime = function() {
	return this._elapsedTime;
};
//-------------------------------------------------------------------------------------------------------------
RLTWheelElement.prototype.resetElapsedTime = function() {
	this._elapsedTime = 0;
};
//-------------------------------------------------------------------------------------------------------------
RLTWheelElement.prototype.setCurrentFrame = function(number) {
	this.currentFrame = number;
	this.setDisplayFrameWithAnimationName(this.spriteFrameName, number);
}
//-------------------------------------------------------------------------------------------------------------
RLTWheelElement.prototype.setSkipFrame = function(skipFrame)
{
	this.skipFrame = skipFrame;
	this._interpolationSkipFrame = skipFrame;
	this._koeffInterpolationSkipFrame = this._baseKoeffInterpolationSkipFrame * this._interpolationSkipFrame;
}
//-------------------------------------------------------------------------------------------------------------
RLTWheelElement.prototype.InSignalFrame = function()
{
	// текущий кадр является сигнальным, если delta = -4, -3, -2, -1 or 0
	var delta;
	if(this.signalFrame > kMaxSkipFrame)
		delta = this.currentFrame - this.signalFrame;
	else
		delta = 180 - this.currentFrame + this.signalFrame;
	if(delta <= 0 && delta >= -kMaxSkipFrame)
	{
		this.skipFrame = Math.abs(delta);
		cc.log("skip frame = {0}".f(this.skipFrame));
		return true;
	}
	return false;
}
//-------------------------------------------------------------------------------------------------------------
RLTWheelElement.prototype.reStartDelay = function()
{
	this.delay = this.defaultDelay;
	this.lawDelay = null;
	this._elapsedTime = 0;
	this.signalFrame = -1;
}