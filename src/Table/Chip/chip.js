var RLTChip = cc.Sprite.extend({
	ctor: function(color, value){
		this._super();
	
		this.chip = cc.Sprite.createWithSpriteFrame(window.RLTResourceManager.spriteFrameChipColor(color));
		this.addChild(this.chip);
		
		this.labelValue = window.RLTResourceManager.labelFontNominals();
		this.labelValue.setString("{0}".f(value));
		this.labelValue.setPosition(cc.pMult(cc.p(0, 2), 1));
		this.labelValue.setAlignment(cc.TEXT_ALIGNMENT_CENTER);
		this.labelValue.setScale(this.scaleLabelChip(this.labelValue.getString()));
		this.addChild(this.labelValue);
		
		var _colorChip = color;
		var _value = 0;
		Object.defineProperties(this, {
			colorChip: {
				set: function(color) {
					_colorChip = color;
					this.chip.setSpriteFrame(window.RLTResourceManager.spriteFrameChipColor(_colorChip));
				},
				get: function() {
					return _colorChip;
				}
			},
			value: {
				set: function(newValue) {
					_value = newValue;
					this.labelValue.setString("{0}".f(newValue));
					this.labelValue.setScale(this.scaleLabelChip(this.labelValue.getString()));

					if(_value == 1)
						this.labelValue.setPosition(cc.pMult(cc.p(-2, 2), 1));
					else
						this.labelValue.setPosition(cc.pMult(cc.p(0, 2), 1));
					this.labelValue.setVisible(_value != 0);					
				},
				get: function() {
					return _value;
				}
			}						
		});

		this.chipColor = color;
		this.getBaseColor = function() {
			return color;
		}
		this.value = value;
	},
	
	/** @brief скейл-фишки в зависимости от удалености по Y */
	scaleChip: function(yCoord, scaleX, scaleY) {
		
	}
});
//----------------------------------------------------------------------------------------------------------------
RLTChip.prototype.resetColor = function()
{
	//this.colorChip = this.getBaseColor();
}
//----------------------------------------------------------------------------------------------------------------
RLTChip.prototype.scaleLabelChip = function(stringValue)
{
	var scaleOfLength = [ 1., 1., 0.97, 0.82, 0.65 ];
	return scaleOfLength[stringValue.length];
}
//----------------------------------------------------------------------------------------------------------------
