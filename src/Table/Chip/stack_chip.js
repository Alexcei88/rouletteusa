//максимальное количество фишек в башенке
var kMAX_NUMBER_IN_TOWER = 9;
//расстояние между фишками в башеньке
var kDistanceChip = 4;

var RLTStackChip = cc.Node.extend({
	ctor: function(ratesNominals, colorNominalDict) {
		
		this._super();
		
		//----------------------------------------------------------------------------------------------------------------
		this.popUpChipValue = function() {
			var arrayChildren = this.getChildren();
			if(arrayChildren.length > 0)
			{
				var chip = arrayChildren[arrayChildren.length - 1];
				for(var i = 0; i < ratesNominals.length; ++i)
				{
					var rate = ratesNominals[i];
					if(colorNominalsDict[rate] == chip.colorChip)
						return rate;
				}
				return 0;
			}
			else
				return 0;
		}
		//----------------------------------------------------------------------------------------------------------------
		//заменяет фишки в башенке, по принципу: две по 5 на одну по 10, и тп
		//сокращая таким образом высоту башенки
		this.transformChip = function(value) {
			var newArrayChipValue = [];

			//---------------------------------------
			var currentMoney = value;

			var maxDifferenceRates = ratesNominals.length;
			var index = 1;
			var currentRate = 0;

			while(currentMoney > 0)
			{
				currentRate = ratesNominals[maxDifferenceRates - index];
				var numChips = Math.floor(currentMoney/currentRate);
				if(numChips > 0)
				{
					for(var i = 0; i < numChips; ++i)
						newArrayChipValue.push(currentRate);
					var moneyThisChips = numChips * currentRate;
					currentMoney -= moneyThisChips;
				}
				++index;
			}
			return newArrayChipValue;
		}
		//----------------------------------------------------------------------------------------------------------------
		// переструктуризация фишек согласно новому массиву фишек
		this.reStructuringStackChip = function(newArrayValueChip)
		{			
			// берем сначала всех детей в списке
			var currentChild = this.getChildren();
			var countAddChip = newArrayValueChip.length - (currentChild == null ? 0 : currentChild.length);
			if(countAddChip > 0)
			{
				// нужны новые фишки
				for(var counterAddChip = 0; counterAddChip < countAddChip; ++counterAddChip)
				{
					var chip = new RLTChip(RLT_CHIP_COLOR.CHIP_BLUE, 1);
					this.addChild(chip);
					chip.setAnchorPoint(cc.p(0, 1));
					// позицию меняем только после добавления узла в стэк
					chip.setPosition(cc.p(0, 0));
				}
			}
			else if(countAddChip < 0)
			{
				// последние фишки убираем из стэка
				var count = 1;
				for(var counterRemoveChip = countAddChip; counterRemoveChip != 0; ++counterRemoveChip)
				{
					var child = currentChild[currentChild.length - count++];
					this.removeChild(child);
				}
			}
			else
			{
				// есть нужное количество фишек
			}

			// берем новое количество детей в списке
			var newChild = this.getChildren();

			// получившее значение фишек должно совпадать с массивом значений
			cc.assert(newChild.length == newArrayValueChip.length, "В стэке находится не то количество фишек, которое необходимо для отображения");

			// раставляем фишки согласно новому массиву
			// меняем цвет фишек согласно новому массиву значений
			for(var i = 0; i < newChild.length; ++i)
			{
				var chip = newChild[i];
				var newColor = colorNominalDict[newArrayValueChip[i]];
				if(newColor != chip.colorChip)
					chip.colorChip = newColor;

				if(i != 0)
				{
					var chipPrev = newChild[i - 1];
					chip.setPosition(cc.pAdd(chipPrev.getPosition(), cc.p(0, kDistanceChip)));
				}
				// устанавливаем значение у последней фишки то, которое нужно, иначе zero
				chip.value = (i == newChild.length - 1) ? this.value : 0;
			}
		}		
	},
	value: 0
});
//----------------------------------------------------------------------------------------------------------------
RLTStackChip.prototype.push = function(value) {
	// минимизируем количество фишек в стеке
	var newArrayValueChip = this.transformChip(this.value + value);
	if(newArrayValueChip.length > kMAX_NUMBER_IN_TOWER){
		//  превышен лимит фишек в стеке
		return false;
	}
	cc.log("push newValue in stack: %d", value);
	this.value += value;
	this.reStructuringStackChip(newArrayValueChip);
	//if(self.soundOn)
	//	[_soundAddChip play];
	return true;
}
//----------------------------------------------------------------------------------------------------------------
RLTStackChip.prototype.pop = function(value) {
	this.value -= value;
	// минимизируем количество фишек в стеке
	var newArrayValueChip = this.transformChip(this.value);
	this.reStructuringStackChip(newArrayValueChip);
	//if(self.soundOn)
	//	[_soundUnAddChip play];
}
//----------------------------------------------------------------------------------------------------------------
/** @brief начать полет фишек из стэка с паузой в заданную точку */
RLTStackChip.prototype.startFlyChips = function(time, pause, targetPoint) {
	
}
//----------------------------------------------------------------------------------------------------------------
/** @brief очистить стэк от фишек */
RLTStackChip.prototype.clearChips = function() {
	this.removeAllChildren();
	this.value = 0;
}
//----------------------------------------------------------------------------------------------------------------


