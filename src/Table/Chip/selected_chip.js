var RLT_SELECTED_CHIP_STATE = {};
RLT_SELECTED_CHIP_STATE.Enum(
		'RLT_DOWN_STATE',
		'RLT_UP_STATE');


//высота подъема фишки в пикселях, когда на неё навели мышкой
var kHeightUpChip = 12.;
//время подъема фишки
var kTimeHeightUpChip = 0.2;
//смещение тени по оси X при поднятии фишки
var kOffsetXShadow = -2.;

//----------------------------------------------------------------------------------------------------------------
var RTLSelectedChip = RLTChip.extend({
	ctor: function(color, value) {
		this._super(color, 0);
		this.boundingBox = cc.rect(0 - this.chip.getTextureRect().width/2, 0 - this.chip.getTextureRect().height/2, this.chip.getTextureRect().width, this.chip.getTextureRect().height);

		var shadow = cc.Sprite.createWithSpriteFrame(window.RLTResourceManager.spriteFrameShadowChip());
		this.basePositionShadow = shadow.getPosition();
		this.addChild(shadow, this.chip.getLocalZOrder() - 2);
	
		var _animationChip  = window.RLTResourceManager.animChipColor(color);
		_animationChip.setRestoreOriginalFrame(true);
		_animationChip.retain();
		
		var _animationShadow = window.RLTResourceManager.animShadowChip(color);
		_animationShadow.setRestoreOriginalFrame(true);
		_animationShadow.retain();
		
		var _animationLabel = window.RLTResourceManager.animFontForNominal(value);
		_animationLabel.setRestoreOriginalFrame(true);
		_animationLabel.retain();	

		this.valueSelectedChip = value;

		this.label = cc.Sprite.createWithSpriteFrame(window.RLTResourceManager.spriteFrameFontForNominal(value));
		this.addChild(this.label);
		
		var _enable = false;
		var _state = RLT_SELECTED_CHIP_STATE.RLT_DOWN_STATE;
		var _value = 0;
		
		function up()
		{
			_state = RLT_SELECTED_CHIP_STATE.RLT_UP_STATE;

			this.stopAllActions();
			shadow.stopAllActions();

			var koeffScale = 1.;
			
			function moveShadow() {
				shadow.runAction(cc.moveTo(kTimeHeightUpChip, cc.p(this.basePositionShadow.x + kOffsetXShadow/koeffScale, this.basePositionShadow.y - kHeightUpChip/koeffScale))); 
			}
			
			var actionChipAnimate = cc.repeatForever(cc.animate(_animationChip));
			actionChipAnimate.retain();
			
			var actionShadowAnimate = cc.repeatForever(cc.animate(_animationShadow));
			actionShadowAnimate.retain();
			
			var actionLabelAnimate = cc.repeatForever(cc.animate(_animationLabel));
			actionLabelAnimate.retain();

			// функция, которая должна вызываться из деструктора
			this.releaseAnimate = function(){
					actionChipAnimate.release();
					actionLabelAnimate.release();
					actionShadowAnimate.release();
			}
			
			function moveEndBlock() {
				this.chip.stopAllActions();
				this.chip.runAction(actionChipAnimate);

				shadow.stopAllActions();
				shadow.runAction(actionShadowAnimate);

				this.label.stopAllActions();
				this.label.runAction(actionLabelAnimate);

				// освобождаем действия
				actionChipAnimate.release();
				actionShadowAnimate.release();
				actionLabelAnimate.release();
			}
				
			var actionArray = [];
			actionArray.push(cc.spawn(
					cc.moveTo(kTimeHeightUpChip, cc.p(this.basePosition.x, this.basePosition.y + kHeightUpChip/koeffScale)),
					cc.CallFunc.create.call(this, moveShadow, this)
				));
			actionArray.push(cc.CallFunc.create.call(this, moveEndBlock, this));			
			this.runAction(cc.sequence(actionArray));							
		}
		
		function down()
		{
			_state = RLT_SELECTED_CHIP_STATE.RLT_DOWN_STATE;
			
			function moveShadow() {
				shadow.runAction(cc.moveTo(kTimeHeightUpChip, this.basePositionShadow)); 
			}
			
			var shadowFrameDisplay = window.RLTResourceManager.spriteFrameShadowChip();
			shadowFrameDisplay.retain();
			
			var labelFrameDisplay = window.RLTResourceManager.spriteFrameFontForNominal(this.valueSelectedChip);
			labelFrameDisplay.retain();
			
			var chipFrameDisplay = window.RLTResourceManager.spriteFrameChipColor(this.colorChip);
			chipFrameDisplay.retain();

			function moveEndBlock() 
			{
				this.chip.stopAllActions();
				this.label.stopAllActions();

				shadow.setSpriteFrame(shadowFrameDisplay);
				this.label.setSpriteFrame(labelFrameDisplay);
				this.chip.setSpriteFrame(chipFrameDisplay);
				
				shadowFrameDisplay.release();
				labelFrameDisplay.release();
				chipFrameDisplay.release();
			}
	
			this.stopAllActions();
			shadow.stopAllActions();
			
			var actionArray = [];
			actionArray.push(cc.spawn(
					cc.moveTo(kTimeHeightUpChip, this.basePosition),
					cc.CallFunc.create.call(this, moveShadow, this)
				));
			actionArray.push(cc.CallFunc.create.call(this, moveEndBlock, this));			
			this.runAction(cc.sequence(actionArray));							
		}
		
		Object.defineProperties(this, {
			enable: {
				set: function(newValue) {
					if(this.enable == newValue)
						return;
					_enable = newValue;
					if(!this.enable)
						// перекрашиваем фишку в серый цвет
						this.colorChip = RLT_CHIP_COLOR.CHIP_GRAY;
					else
						// перекрашиваем в исходный цвет
						this.resetColor();
				},
				get: function(){
					return _enable;
				}

			},
			/** @brief состояние фишки(поднята, опущена) */
			state: {
				set: function(newState) {
					if(this.state == newState)
						return;
					if(newState == RLT_SELECTED_CHIP_STATE.RLT_DOWN_STATE)
						down.call(this);
					else
						up.call(this);
				},
				get: function() {	
					return _state;
				}
			}			
		});
		
		this.state = RLT_SELECTED_CHIP_STATE.RLT_DOWN_STATE;
		this.enable = false;
	},
	baseScale: 1,	
	/** @brief базовая позиция фишки */
	basePosition: cc.p(0, 0)
});
