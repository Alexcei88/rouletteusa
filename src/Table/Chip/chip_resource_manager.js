var RLT_CHIP_COLOR = {};
RLT_CHIP_COLOR.Enum(
		'CHIP_YELLOW',  // желтый
		'CHIP_GREEN',  // зеленый
		'CHIP_BLUE',  // синий
		'CHIP_ORANGE',  // оранжевый
		'CHIP_RED',  // красный
		'CHIP_PURPLE',  // фиолетовый
		'CHIP_GRAY'  // серый
);

//количество кадро анимации покачивания фишки
var kCountFrameChipSwingAnimation = 20;
//время между переключениями кадров
var kSwitchTimeFrameChipSwingAnimation = 0.035;

window.RLTResourceManager = Object.create(Object.prototype, {
		        
				/** @brief спрайтовый фрейм нужного цвета */
				spriteFrameChipColor: { value: function(color) {	
					return this.chipSpriteFrameChipDict[color].clone();
				} },        
        		/** @brief анимация фишки нужного цвета */
        		animChipColor: {value: function(color) {
        			return this.chipSwingAnimationDict[color].clone();
        		}},

        		/** @brief анимация покачивания шрифта */
        		spriteFrameFontForNominal: { value: function(nominal) {
        			return this.fontSpriteFrameNominalsDict[nominal].clone();
        		} },

        		/** @brief анимация покачивания шрифта */
        		animFontForNominal: { value: function(nominal) {
        			return this.fontSwingAnimationDict[nominal].clone();
        		} },

        		/** @brief спрайт-фрейм тени фишки */
        		spriteFrameShadowChip: { value: function() {
        			return this._spriteFrameShadowChip.clone();
        		} },

        		/** @brief анимация тени фишки */
        		animShadowChip: { value: function() {
        			return this._animShadowChip.clone();
        		} },

        		/** @brief шрифт для номинала фишки */
        		labelFontNominals: { value: function() {
        			return new cc.LabelBMFont("1", res.ChipFnt);
        		} },

        		/** @brief спрайт подсветки фишки */
        		spriteGlowChip: { value: function() {
        			return this.spriteFrameGlowChip.clone();
        		} }
	}
);

(function() {

	function proccessLoadChipSwing()
	{
		cc.log("ProccessLoadChipSwing!");
		var _chipSwingIdentifiers = {};	
		_chipSwingIdentifiers[RLT_CHIP_COLOR.CHIP_BLUE] = "chip_swing_blue_{0}.png";
		_chipSwingIdentifiers[RLT_CHIP_COLOR.CHIP_GREEN] = "chip_swing_gray_{0}.png";
		_chipSwingIdentifiers[RLT_CHIP_COLOR.CHIP_GREEN] = "chip_swing_green_{0}.png";
		_chipSwingIdentifiers[RLT_CHIP_COLOR.CHIP_ORANGE] = "chip_swing_orange_{0}.png";
		_chipSwingIdentifiers[RLT_CHIP_COLOR.CHIP_PURPLE] = "chip_swing_purple_{0}.png";
		_chipSwingIdentifiers[RLT_CHIP_COLOR.CHIP_YELLOW] = "chip_swing_yellow_{0}.png";
		_chipSwingIdentifiers[RLT_CHIP_COLOR.CHIP_RED] = "chip_swing_red_{0}.png";

		var dictTemp = {};
		for(var color in _chipSwingIdentifiers)
		{
			var spriteFrame = cc.spriteFrameCache.getSpriteFrame(_chipSwingIdentifiers[color].format(0));
			cc.assert(spriteFrame != null, "Нет спрайтового фрейма для фишки");
			dictTemp[color] = spriteFrame;
		}
		window.RLTResourceManager.chipSpriteFrameChipDict = dictTemp;	
				
		// загрузка анимации
		dictTemp = {};
		for(var color in _chipSwingIdentifiers)
		{
			var animation = cc.Animation.createAnimation(_chipSwingIdentifiers[color], kCountFrameChipSwingAnimation, kSwitchTimeFrameChipSwingAnimation);
			dictTemp[color] = animation;
		}
		// сохраняем анимацию в словаре
		window.RLTResourceManager.chipSwingAnimationDict = dictTemp;
		
		// загружаем подствеку фишки
		window.RLTResourceManager.spriteFrameGlowChip = cc.spriteFrameCache.getSpriteFrame("chip_glow.png");
	}
	
	//----------------------------------------------------------------------------------------------------------------
	function proccessLoadShadowChip()
	{
		// спрайт-фрейм
		window.RLTResourceManager._spriteFrameShadowChip = cc.spriteFrameCache.getSpriteFrame("chip_shadow_0.png");
		cc.assert(window.RLTResourceManager.spriteFrameShadowChip != null, "Нет спрайтового фрейма для тени фишки");

		// анимация
		window.RLTResourceManager._animShadowChip = cc.Animation.createAnimation("chip_shadow_{0}.png", kCountFrameChipSwingAnimation, 
		kSwitchTimeFrameChipSwingAnimation);	
	}
	//----------------------------------------------------------------------------------------------------------------
	function processLoadFontAnimationChip()
	{
		var arrayNominals = [1, 5, 10, 50, 100, 500];
		// загрузка cпрайтового фрейма фишки для нулевого кадра
		var dictTemp = {};

		for(var i = 0; i < 6; ++i)
		{
			var spriteFrame = cc.spriteFrameCache.getSpriteFrame("text_swing_{0}_{1}.png".f(arrayNominals[i], 0));
			cc.assert(spriteFrame != null, "Нет спрайтового фрейма для шрифта фишки");
			dictTemp[arrayNominals[i]] = spriteFrame;
		}

		window.RLTResourceManager.fontSpriteFrameNominalsDict = dictTemp;

		dictTemp = {};
		// загрузка анимации
		for(var i = 0; i < 6; ++i)
		{
			var nameAnimation = "text_swing_{0}".f(arrayNominals[i]);
			nameAnimation = nameAnimation + "_{0}.png";
			var animation = cc.Animation.createAnimation(nameAnimation, kCountFrameChipSwingAnimation, kSwitchTimeFrameChipSwingAnimation);
			dictTemp[arrayNominals[i]] = animation;
		}
		// сохраняем анимацию в словаре
		window.RLTResourceManager.fontSwingAnimationDict = dictTemp;
	}	
	cc.spriteFrameCache.addSpriteFrames(res.Chips_plist);
	proccessLoadChipSwing();
	proccessLoadShadowChip();
	processLoadFontAnimationChip();
	
})();
