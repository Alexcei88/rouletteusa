
var RLT_OptionFlyChips = {
	// задержка полета фишек(пока игрок проанализирует ситуация)
	delayFly: 0,
	// задержка полета между фишками в стеке
	delayFlyBetweenChips: 0,
	// прямоугольник, куда летят выигрышные фишки
	rectForWinnigChips: null,
	// прямоугольник, куда летят проигрышные фишки
	rectForLossingChips: null,
	// условное время полета выигрышной фишки
	baseTimeFlyWinnigChip: 0,
	// условное время полета проигрышной фишки
	baseTimeFlyLossingChip: 0,
	// максимальное расстояние полета выигрышной фишки
	maxPathFlyWinningChips: 0,
	// максимальное расстояние полета проигрышной фишки
	maxPathFlyLossingChips: 0
};

var RLTTable = cc.Node.extend({
	wheel: null,
	ctor: function(zonePlist, delegate, model) {
		this._super();
		
		// фон
		var sprite = new cc.Sprite(res.Background_png);
		sprite.setAnchorPoint(cc.p(0, 0));
		this.addChild(sprite);	

		// рулетка
		this.wheel = new RLTWheel();
		this.wheel.setAnchorPoint(cc.p(0, 0));
		this.addChild(this.wheel);
				
		// панель выбора номинала для делания ставки
		var nominalsRates = [1, 5, 10, 50, 100, 500];
		var colorNominalsDict = {	  1: RLT_CHIP_COLOR.CHIP_PURPLE, 
		                  			  5: RLT_CHIP_COLOR.CHIP_YELLOW, 
		                  			 10: RLT_CHIP_COLOR.CHIP_GREEN, 
		                  			 50: RLT_CHIP_COLOR.CHIP_BLUE, 
		                  			100: RLT_CHIP_COLOR.CHIP_ORANGE,
		                  			500: RLT_CHIP_COLOR.CHIP_RED
		                  			};
	
		this.selectedChipPanel = new RLTSelectedChipPanel(res.RLT_SelectedChipPanelPlist, nominalsRates, colorNominalsDict, delegate);
		this.setAnchorPoint(cc.p(0,0));
		this.addChild(this.selectedChipPanel);
		
		this.arrayZone = [];

		// словарь соответствия выигранного номера и индекса зоны
		var _dictIndexZoneForWinNumber = [];
		
		// загрузка разметки стола
		function loadTableZone(dict, colorNominalsDict, nominalsRates) 
		{
			cc.spriteFrameCache.addSpriteFrames(res.LightZone_plist);
			
			var rotation = +dict["rotation"];
			var arrayZone = dict["zone"];
			cc.assert(arrayZone != null, "Нет массива зон в файле описания зон стола");
			
			var dictIndexZoneForWinNumber = [];
			// cначала добавляем все подсвеченные картинки у зон(они должны все лежать слоем ниже фишек)
			var pressedStateArray = [];

			for(var index = 0; index < arrayZone.length; ++index)
			{
				var dictZone = arrayZone[index];
				// создаем узел из спрайтов для подсвечивания области
				var pressedState = new cc.Node();
				pressedState.setAnchorPoint(cc.p(0, 1));
				pressedState.setPosition(cc.p(0, 0));
				pressedState.setVisible(false);
				
				var selectedSprite = cc.Sprite.createWithSpriteFrameName(dictZone["nameframe"]);
				selectedSprite.setAnchorPoint(cc.p(0, 1));
				selectedSprite.setPosition(cc.pointFromString(dictZone["position"]));
				pressedState.addChild(selectedSprite);
				
				var arraySelectAddZone = dictZone["selectedaddzone"];
				if(arraySelectAddZone)
				{
					// добавляем дополнительные зоны, которые необходимо показывать при наведении на текущую зону
					for(var i = 0; i < arraySelectAddZone.length; ++i)
					{
						var dictAddZone = arrayZone[arraySelectAddZone[i]];
						var selectedAddSprite = cc.Sprite.createWithSpriteFrameName(dictAddZone["nameframe"]);
						selectedAddSprite.setAnchorPoint(cc.p(0, 1));
						selectedAddSprite.setPosition(cc.pointFromString(dictAddZone["position"]));
						pressedState.addChild(selectedAddSprite);
					}
				}
				
				this.addChild(pressedState);
				pressedStateArray[index] = pressedState;				
			}
			
			var selectedChipPanel = this.selectedChipPanel;
			var model = this.getModel();
			for(index = 0; index < arrayZone.length; ++index)
			{
				var dictZone = arrayZone[index];
				var rotationOverload  = +dictZone["rotation"];
				var rotationZone = rotationOverload ? rotationOverload : rotation;
				var skewX = +dictZone["skew"];				
				var zonePosition = cc.pointFromString(dictZone["position"]);				
				var stackZonePositionOffset = cc.pointFromString(dictZone["stack_chip_offset"]);
				
				// создаем стэк фишек для зоны
				var stackChip = new RLTStackChip(nominalsRates, colorNominalsDict);
				stackChip.setPosition(cc.pAdd(zonePosition, stackZonePositionOffset));
				this.addChild(stackChip, index);
				
				// создаем область
				var zone = new RLTTableZone(pressedStateArray[index], stackChip, index, function(sender) {
					cc.log("Execute function");
					var selectZone = sender;
					if(selectedChipPanel.value <= 0)
						return;
					
					selectedChipPanel.removeMode 
							? model.RemoveBet(selectZone.index, selectZone.highChipValue)
							: model.SetBet(selectZone.index, selectedChipPanel.value);
				});
				
				var sourceRect = cc.rectFromString(dictZone["active_area"]);
				zone.setActiveArea(sourceRect);				
				zone.setPosition(zonePosition);
				zone.setAnchorPoint(cc.p(0, 1));
//				if(index > 153)
//				{
//					cc.log("rotation: %d", rotationZone);
//					cc.log("skew: %d", skewX ?  skewX : rotationZone);
//					cc.log("activeArea: %d, %d, %d, %d", zone.activeArea.x, zone.activeArea.y, zone.activeArea.width, zone.activeArea.height);
//					cc.log("position: %d, %d",  zonePosition.x, zonePosition.y);
//				}
				zone.setRotation(rotationZone);				
				zone.setSkewX(skewX ?  skewX : rotationZone);
				this.arrayZone.push(zone);
				
				// посылаем модели информацию для делания ставки в созданном поле
				var numberBetsArray = dictZone["numbers_of_bet"];
				model.InitField(index, numberBetsArray);
				if(numberBetsArray.length == 1)
				{
					dictIndexZoneForWinNumber[numberBetsArray[0]] = this.arrayZone.count;
				}
			}	
			
			this.menuZone = new cc.MenuExtension(this.arrayZone);
			this.menuZone.setAnchorPoint(cc.p(0, 1));
			this.menuZone.setPosition(cc.p(0, 0));
			this.addChild(this.menuZone);
			
		}
		
		this.getViewDelegate = function() {
			return delegate;
		}
		this.getModel = function() {
			return model;
		}
		loadTableZone.call(this, zonePlist, colorNominalsDict, nominalsRates);						
		
		function loadOptionFlyChips(showingWinDict)
		{
			RLT_OptionFlyChips.delayFlyBetweenChips = showingWinDict["delay_fly_between_chips"];
			RLT_OptionFlyChips.delayFly = showingWinDict["delay_fly_time"];
			RLT_OptionFlyChips.rectForWinnigChips = cc.rectFromString(showingWinDict["target_rect_for_winning_chips"]);
			RLT_OptionFlyChips.rectForLossingChips = cc.rectFromString(showingWinDict["target_rect_for_lossing_chips"]);
			RLT_OptionFlyChips.baseTimeFlyWinnigChip = showingWinDict["basetime_fly_winning_chips"];
			RLT_OptionFlyChips.baseTimeFlyLossingChip = showingWinDict["basetime_fly_lossing_chips"];

			//  высчитываем максимальный путь от 0 зоны(она самая дальняя)
			var zone = this.arrayZone[0];
			RLT_OptionFlyChips.maxPathFlyLossingChips = cc.pDistance(zone.stackChips.getPosition(), cc.pMidpoint(RLT_OptionFlyChips.rectForLossingChips, cc.p(RLT_OptionFlyChips.rectForLossingChips.x + RLT_OptionFlyChips.rectForLossingChips.width, RLT_OptionFlyChips.rectForLossingChips.y - RLT_OptionFlyChips.rectForLossingChips.height)));
			RLT_OptionFlyChips.maxPathFlyWinningChips = cc.pDistance(zone.stackChips.getPosition(), cc.pMidpoint(RLT_OptionFlyChips.rectForWinnigChips, cc.p(RLT_OptionFlyChips.rectForWinnigChips.x + RLT_OptionFlyChips.rectForWinnigChips.width,  RLT_OptionFlyChips.rectForWinnigChips.y - RLT_OptionFlyChips.rectForWinnigChips.height)));
		}
		
		loadOptionFlyChips.call(this, cc.plistParser.parse(jsb.fileUtils.getStringFromFile(res.RLT_ShowWinDescriptionPlist)));
		
		var _enable = true;
		Object.defineProperties(this, {
			enable: {
				set: function(newValue) {
					_enable = newValue;
					this.menuZone.setEnabled(newValue);
					for(var i = 0; i < this.arrayZone.length; ++i)
						this.arrayZone[i].enabledSelected = newValue;
				},
				get: function() {
					return _enable;
				}
			}				
		});
	},
	onEnter: function()	{
		this._super();
		
		if( 'touches' in sys.capabilities ) {
			cc.log("touch Enabled");
			//this.setTouchEnabled(true);
		}
		else if( 'mouse' in sys.capabilities ) {
			cc.eventManager.addListener({
				event: cc.EventListener.MOUSE,
				onMouseMove: function(event){
					var target = event.getCurrentTarget();
					target.onMouseMove(event);
					
				},
				onMouseUp: function(event){
					var target = event.getCurrentTarget();
					target.onMouseUp(event);
				},
				onMouseDown: function(event){
					var target = event.getCurrentTarget();
					target.onMouseDown(event);
				}				
			},this);

		}
	},

	update: function(time) {
		this._super(time);
		this.wheel.update(time);
	},
	selectedChipPanel: null
});
//----------------------------------------------------------------------------------------------------------------
RLTTable.prototype.onMouseDown = function(event) {
	this.selectedChipPanel.onMouseDown(event);
};
//----------------------------------------------------------------------------------------------------------------
RLTTable.prototype.onMouseMove = function(event) {
	this.selectedChipPanel.onMouseMove(event);
};
//----------------------------------------------------------------------------------------------------------------
RLTTable.prototype.onMouseUp = function(event) {
	this.selectedChipPanel.onMouseUp(event);
}
//----------------------------------------------------------------------------------------------------------------
RLTTable.prototype.updateSelectedChips = function()
{
	this.selectedChipPanel.updateActiveChips(this.getModel().GetCredits() - this.getModel().GetTotalBet());
}
//----------------------------------------------------------------------------------------------------------------
RLTTable.prototype.didMakeBet = function(field, value) 
{
	this.updateSelectedChips();
	return this.arrayZone[field].makeBet(value);
}
//----------------------------------------------------------------------------------------------------------------
RLTTable.prototype.didCancelBet = function(field, value)
{
	this.updateSelectedChips();
	this.arrayZone[field].cancelBet(value);
}
//----------------------------------------------------------------------------------------------------------------
RLTTable.prototype.clearAllChips = function()
{
	for(var i = 0; i < this.arrayZone.length; ++i)
		this.arrayZone[i].clearStackChips();
}
//----------------------------------------------------------------------------------------------------------------
RLTTable.prototype.changeBet = function(number)
{
	this.selectedChipPanel.number = number;
}
//----------------------------------------------------------------------------------------------------------------
RLTTable.prototype.startFlyChips = function()
{
	this._winningField = this.getModel().GetWinningFields();
	this._lossingField = this.getModel().GetLosingFields();
	
	//this.scheduler.scheduleCallbackForTarget(this, startFlyLossingChips, 1, 1, 1.0, false);
}
//----------------------------------------------------------------------------------------------------------------
RLTTable.prototype.startFlyWinningChips = function()
{
	/*
	 * 
	 NSLog(@"%s", __PRETTY_FUNCTION__);

	ccTime maxTime = 0.00001f;

	// начинаем полет проигрышных фишек
	for(NSNumber* number in _winningField)
	{
		RLT_TableZone* zone = _arrayZone[[number unsignedIntegerValue]];
		CGPoint targetPoint = [self targetPointInRect:_optionFlyChip.rectForWinnigChips];
		ccTime time = [self timeFlyWinningChipStartPoint: zone.stackPosition toTargetPoint:targetPoint];
		[zone startFlyChipsWithTime:time withPause:_optionFlyChip.delayFlyBetweenChips toPoint: targetPoint];
		if((time + (zone.count - 1) * _optionFlyChip.delayFlyBetweenChips) > maxTime)
			maxTime = time + (zone.count - 1) * _optionFlyChip.delayFlyBetweenChips;
	}
	[self performSelector:@selector(endFlyChip) withObject:self afterDelay:maxTime];
	 */
}
//----------------------------------------------------------------------------------------------------------------
RLTTable.prototype.startFlyLossingChips = function()
{
	cc.log("start fly lossing chip");
/*
	[self reOrderZoneBeforeFly];

	ccTime maxTime = 0.00001f;

	// начинаем полет проигрышных фишек
	for(NSNumber* number in _lossingField)
	{
		RLT_TableZone* zone = _arrayZone[[number unsignedIntegerValue]];
		CGPoint targetPoint = [self targetPointInRect:_optionFlyChip.rectForLossingChips];
		ccTime time = [self timeFlyLossingChipStartPoint:zone.stackPosition toTargetPoint:targetPoint];
		[zone startFlyChipsWithTime:time withPause:_optionFlyChip.delayFlyBetweenChips toPoint: targetPoint];
		if((time + (zone.count - 1) * _optionFlyChip.delayFlyBetweenChips) > maxTime)
			maxTime = time + (zone.count - 1) * _optionFlyChip.delayFlyBetweenChips;
	}
	[self performSelector:@selector(startFlyWinningChip) withObject:self afterDelay:maxTime];*/
}

