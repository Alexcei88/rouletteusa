var RLTSelectedChipPanel = cc.Node.extend({
	ctor: function(plist, nominalsRates, colorNominalsDict, delegate){
		this._super();
		
		var dict = cc.plistParser.parse(jsb.fileUtils.getStringFromFile(plist));
		var startPoint = cc.pointFromString(dict["startPoint"]);
		var distancePoint = cc.pointFromString(dict["distanceBetweenChip"]);

		this.fixScale = dict["scaleChip"];
		
		this.glowChip = cc.Sprite.createWithSpriteFrame(window.RLTResourceManager.spriteGlowChip());
		this.glowChip.setPosition(startPoint);
		this.glowChip.setVisible(false);
		this.glowChip.setScale(this.fixScale);
		this.addChild(this.glowChip);
		
		this.arrayChips = [];
		for(var nominal = 0; nominal < nominalsRates.length; ++nominal)
		{
			var chip = new RTLSelectedChip(colorNominalsDict[nominalsRates[nominal]], nominalsRates[nominal]);
			chip.setAnchorPoint(cc.p(0, 0));
			chip.baseScale = this.fixScale;
			this.addChild(chip);
			
			chip.setPosition(cc.pAdd(startPoint, cc.pMult(distancePoint, nominal + 1)));
			chip.basePosition = chip.getPosition();
			
			this.arrayChips.push(chip);
		}
		this.delegate = delegate;
		
		var _value = -1;
		var _removeMode = false;
		Object.defineProperties(this, {
			/** @brief последний выбранный индекс фишки */
			lastIndex: {
				value: -1,
				writable: true
			},
			/** @brief выбранный номинал фишки */
			value: {
				set: function(newValue) {
					// находим фишку с требуемым номиналом
					if(newValue == -1)
					{
						this.selectedChip = null;
						this.lastIndex = -1;
						_value = newValue;
						return;
					}
					var number = 0;
					for(var i = 0; i < this.arrayChips.length; ++i)
					{
						var chip = this.arrayChips[i];
						if(chip.valueSelectedChip == newValue)
						{
							this.selectedChip = chip;
							this.lastIndex = number;
							break;
						}
						++number;
					}
					cc.assert(this.selectedChip != null, "Нет фишки требуемого номинала");
					this.removeMode = false;

					for(var i = 0; i < this.arrayChips.length; ++i)
						this.arrayChips[i].state = RLT_SELECTED_CHIP_STATE.RLT_DOWN_STATE;

					if(this.selectedChip.state != RLT_SELECTED_CHIP_STATE.RLT_UP_STATE)
					{
						this.selectedChip.state = RLT_SELECTED_CHIP_STATE.RLT_UP_STATE;
						_value = this.selectedChip.valueSelectedChip;
						//if(self.playSound)
						//	[_changeBet play];
						this.delegate.didChangeBet(_value);
					}
				},
				get: function() {
					return _value;
				}
			},
			/** @brief режим удаления фишек из стэка */
			removeMode: { 
				set: function(mode) {
					if(this.removeMode == mode)
						return;
					_removeMode = mode;
					if(this.removeMode)
						this.number = -1;
				},
				get: function() {
					return _removeMode;
				}
			},
			/** @brief индекс выбранной фишки */
			number: {
				set: function(newNumber){
					for(var i = 0; i < this.arrayChips.length; ++i)
						this.arrayChips[i].state = RLT_SELECTED_CHIP_STATE.RLT_DOWN_STATE;

					this.selectedChip = null;

					if(newNumber == -1)
					{
						this.lastIndex = -1;
						this.value = -1;
						this.delegate.didChangeBet(-1);
						return;
					}

					if(newNumber >= this.arrayChips.length)
					{
						this.lastIndex = -1;
						return;
					}
					this.removeMode = false;
					this.selectedChip = this.arrayChips[newNumber];
					this.lastIndex = newNumber;

					if(this.selectedChip.state != RLT_SELECTED_CHIP_STATE.RLT_UP_STATE)
					{
						this.selectedChip.state = RLT_SELECTED_CHIP_STATE.RLT_UP_STATE;
						this.value = this.selectedChip.valueSelectedChip;
						//if(self.playSound)
						//	[_changeBet play];
						this.delegate.didChangeBet(this.value);
					}
				}
			}
		});

		this.value = 1;
		this.handled = false;				
		this.removeMode = false;
	},

	/** @brief был клик по фишке */
	clickChip: false,

	enable: false,
	selectedChip: null

	//@property (nonatomic, assign) BOOL playSound;
});
//----------------------------------------------------------------------------------------------------------------
/** @brief обновление активных фишек в зависимости от значения кредита */
RLTSelectedChipPanel.prototype.updateActiveChips = function(credits) {
	cc.log("updateActiveChips!");
	cc.log("Credit:", credits);
	var saveIndex = this.lastIndex;
	// максимальный индекс, до которого возможно поднять величину
	var maxIndex = 0;
	// уменьшааем ставку до максимально возможного
	for(var i = this.arrayChips.length - 1; i >= 0; --i)
	{
		var chip = this.arrayChips[i];
		chip.enable = credits >= chip.value;

		if(maxIndex == 0 && chip.enable)
			maxIndex = i;

		if(chip.enable == false && i == this.lastIndex)
		{
			// фишка недоступна для ставок, уменьшаем ставку
			if(this.lastIndex != 0)
				this.number  = this.lastIndex - 1;
		}
	}

	this.lastIndex = saveIndex;
	// восстанавливем ставку до максимально возможного значения
	if(this.lastIndex != -1)
	{
		var chip = this.arrayChips[this.lastIndex];
		//  если фишка с максимальным номером не выбрана, то пытаемся восстановить ставку до этого состояния
		if(chip.state == RLT_SELECTED_CHIP_STATE.RLT_DOWN_STATE)
		{
			for(var i = this.lastIndex < maxIndex ? this.lastIndex : maxIndex; i >= 0; --i)
			{
				var chip = this.arrayChips[i];
				if(chip.enable)
				{
					if(chip.state == RLT_SELECTED_CHIP_STATE.RLT_DOWN_STATE)
						this.number  = i;
					break;
				}
			}
		}
	}
	this.lastIndex = saveIndex;
};
//----------------------------------------------------------------------------------------------------------------
RLTSelectedChipPanel.prototype.onMouseDown = function(event) {
	if(this.enable)
		this.updateGlowChip(event);
};
//----------------------------------------------------------------------------------------------------------------
RLTSelectedChipPanel.prototype.onMouseMove = function(event) 
{
	if(this.enable)
		this.updateGlowChip(event);
};
//----------------------------------------------------------------------------------------------------------------
RLTSelectedChipPanel.prototype.onMouseUp = function(event) 
{	
	if(this.handled && this.enable)
	{
		this.clickChip = true;
		cc.log("lastIndex: ", this.lastIndex);
		this.number = this.lastIndex;
		
		this.glowChip.setVisible(false);
	}
}
//----------------------------------------------------------------------------------------------------------------
RLTSelectedChipPanel.prototype.updateGlowChip = function(event)
{
	for(var i = 0; i < this.arrayChips.length; ++i)
	{
		var chip = this.arrayChips[i];
		var locationInNode = chip.convertToNodeSpace(event.getLocation());
		this.handled =  cc.rectContainsPoint(chip.boundingBox, locationInNode);
		if(this.handled && chip.state == RLT_SELECTED_CHIP_STATE.RLT_DOWN_STATE && chip.enable)
		{
			this.selectedChip = chip;
			this.lastIndex = i;
			this.glowChip.setPosition(chip.getPosition());
			break;
		}
	}

	if(!this.handled)
		this.selectedChip = null;

	this.glowChip.setVisible(this.handled);
}
//----------------------------------------------------------------------------------------------------------------

