var RLTTableZone = cc.MenuItem.extend({
	ctor: function(selectedNode, stackChips, index, callback) {
		this._super(callback, this);
		
		this.stackChips = stackChips;
		this._selectedNode = selectedNode;
		this.index = index;
		
		Object.defineProperties(this, {
			/** @brief выбранный номинал фишки */
			highChipValue: {
				// номинал верхней фишки в стеке
				set: function(newValue) {
					
				},
				get: function() {
					return this.stackChips.popUpChipValue();
				}
			},
			// количество фишек в стеке
			count: {
				set: function(newValue) {

				},
				get: function() {
					return this.stackChips.getChildren().length;
				}
			},
			// текущая ставка на зоне
			bets: {
				set: function(newValue) {

				},
				get: function() {
					return this.stackChips.value;
				}
				
				
			}
		});
	},
	setActiveArea: function(activeArea) {
		this.activeArea = activeArea;
	},
	// индекс зоны
	index: 0,
	// ставка на зоне
	bets: 0,
	// позиция стэка в зоне
	stackPosition: 0,
	// стэк фишек
	stackChips: null,

	selected: function() {
		cc.MenuItem.prototype.selected.call(this);
		if(this.isEnabled())
			this._selectedNode.setVisible(true);
	},
	
	unselected: function() {
		cc.MenuItem.prototype.unselected.call(this);
		if(this.isEnabled())	
			this._selectedNode.setVisible(false);
	}
});
//----------------------------------------------------------------------------------------------------------------
/** @brief  будет реверт ставки */
RLTTableZone.prototype.willRebetBets = function(){
	
}
//----------------------------------------------------------------------------------------------------------------
/** @brief был реверт ставки */
RLTTableZone.prototype.didRebetBets = function() {
	
}
//----------------------------------------------------------------------------------------------------------------
/** @brief сделать ставку */
RLTTableZone.prototype.makeBet = function(value) {
	return this.stackChips.push(value);
}
//----------------------------------------------------------------------------------------------------------------
/** @brief убрать ставку */
RLTTableZone.prototype.cancelBet = function(value) {
	if(this.bets <= 0)
		return;
	this.stackChips.pop(value);
}
//----------------------------------------------------------------------------------------------------------------
/** @brief начать полет фишек из стэка с паузой в заданную точку */
RLTTableZone.prototype.startFlyChips = function(time, pause, targetPoint) {
	
}
//----------------------------------------------------------------------------------------------------------------
/** @brief очистить стэк фишек */
RLTTableZone.prototype.clearStackChips = function() {
	this.stackChips.clearChips();
}
//----------------------------------------------------------------------------------------------------------------
