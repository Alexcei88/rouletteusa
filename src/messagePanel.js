
//длительность появления/скрытия таблички через альфу
var RLTTimeAlphaMSG = 0.22;
var RLTMessageType = {};

RLTMessageType.Enum(
		'MSG_NONE',
		'MSG_BETS_ARE_LIMITED',
		'MSG_DISCONNECT'      ,
		'MSG_MAKE_YOU_BETS'   ,
		'MSG_MAX_STACK_ON_FIELD',
		'MSG_MAX_STACK_ON_TABLE',
		'MSG_NO_MORE_BETS'      ,
		'MSG_NOT_ENOUGH_CREDITS',
		'MSG_SHOW_WIN'          
);

var RLTMessagePanel = cc.Node.extend({
	ctor: function(){
		this._super();
		
		var panel = cc.Sprite.createWithSpriteFrameName("message_panel.png");
		panel.setAnchorPoint(cc.p(0, 1));
		panel.setPosition(cc.p(493, 758));
		this.addChild(panel);

		// позиция текстовой метки по-умолчанию
		this.positionMsgDefault = cc.p(197, 40);

		// шрифты		
		// обычное сообщение
		this.messageLabel = new cc.LabelBMFont("M", res.MessagePanelMessageLabelFnt);
		this.messageLabel.setVisible(false);
		this.messageLabel.setPosition(this.positionMsgDefault);
		panel.addChild(this.messageLabel);
			
		// сообщение о диссконекте		
		this.disconnectLabel = new cc.LabelBMFont("M", res.MessagePanelRedMessageLabelFnt);
		this.disconnectLabel.setVisible(false);
		this.disconnectLabel.setPosition(this.positionMsgDefault);
		panel.addChild(this.disconnectLabel);

		// таймер
		this.timerLabel = new cc.LabelBMFont("5", res.MessagePanelTimerLabelFnt);
		this.timerLabel.setVisible(false);
		this.timerLabel.setAlignment(cc.TEXT_ALIGNMENT_CENTER);
		panel.addChild(this.timerLabel);

		var textMsgIdentifiers = {};
		textMsgIdentifiers[RLTMessageType.MSG_NONE] = "";		
		textMsgIdentifiers[RLTMessageType.MSG_BETS_ARE_LIMITED] = "BETS ARE LIMITED";
		textMsgIdentifiers[RLTMessageType.MSG_DISCONNECT] = "DISCONNECT";
		textMsgIdentifiers[RLTMessageType.MSG_SHOW_WIN] = "SHOW WIN";
		textMsgIdentifiers[RLTMessageType.MSG_MAKE_YOU_BETS] = "MAKE YOUR BETS";
		textMsgIdentifiers[RLTMessageType.MSG_MAX_STACK_ON_FIELD] = "MAXIMUM STAKE \n  ON THE FIELD";
		textMsgIdentifiers[RLTMessageType.MSG_MAX_STACK_ON_TABLE] = "MAXIMUM STAKE \n     ON THE TABLE";
		textMsgIdentifiers[RLTMessageType.MSG_NO_MORE_BETS] = "NO MORE BETS";
		textMsgIdentifiers[RLTMessageType.MSG_NOT_ENOUGH_CREDITS] = "NOT ENOUGH \n    CREDITS";
		
		this.getTextForMessage = function(type) {
			return textMsgIdentifiers[type];
		}
		
		this.startTime = null;
		
		this.clearTimerLabel = function(){
			if(this.timerID != null)
				clearInterval(this.timerID);
		}
		
		this.updateTimerLabel = function() {
			this.startTime = new Date();
			var object = {
				func: function() 
				{ 
					var diffTime = new Date() - this.self.startTime;
					var time = this.self.timeMakeYourBets * 1000 - diffTime;
					if(time <0)
					{
						var actionsHide = [];
						this.self.timerLabel.setString("0");
						// считываем действия для скрытия предыдущей надписи
						this.self.slowHideMessageAction(actionsHide, this.self.timerLabel);
						this.self.timerLabel.runAction(cc.sequence(actionsHide));
						this.self.clearTimerLabel.call(this.self);
					}
					else
						this.self.timerLabel.setString(Math.ceil(time/1000));
				},
				self: null			
			}
			object.self = this;
			
			this.timerID = setInterval(function() {
				object.func.call(object);
			}, 200)
		}
		
		//----------------------------------------------------------------------------------------------------------------
		// функции возвращают действия для плавного появления/скрытия табличек
		this.slowHideMessageAction = function(actions, font)
		{
			var temp = function() {
				font.setOpacity(255);
			};
			actions.push(cc.CallFunc(temp, this, null));
			actions.push(cc.FadeOut(RLTTimeAlphaMSG).easing(cc.EaseSineIn));
			actions.push(cc.Hide());			
		}
		//----------------------------------------------------------------------------------------------------------------
		this.slowShowMessageAction = function(actions, font)
		{
			var temp = function() {
				font.setOpacity(0);
			};

			actions.push(cc.CallFunc(temp, this, null));
			actions.push(cc.Show());			
			actions.push(cc.FadeIn(RLTTimeAlphaMSG).easing(cc.EaseSineIn));
		}		
		//----------------------------------------------------------------------------------------------------------------
		this.getPositionForMessage = function(messageType)
		{
			var doc = cc.plistParser.parse(jsb.fileUtils.getStringFromFile(res.MessagePanelDescriptionPlist));
			var dict = doc["Message"][messageType];
			var pointOffset = cc.p(0, 0);
			if(dict)
				pointOffset = cc.pointFromString(dict["offset_position"]);				
			return cc.pAdd(this.positionMsgDefault, pointOffset);
		}
		return true;
	},
		
	currentMsgId: RLTMessageType.MSG_NONE,
	timeMakeYourBets: 0,
	timerID: null
});
//----------------------------------------------------------------------------------------------------------------
/** @brief по идентификатору показывает сообщение */
RLTMessagePanel.prototype.showMessage = function(type, userInfo) {
	if(this.currentMsgId == type)
		return;
	
	var currentFont = this.messageLabel;
	if(this.currentMsgId == RLTMessageType.MSG_DISCONNECT)
		currentFont = this.disconnectLabel;

	var newFont = this.messageLabel;
	if(type == RLTMessageType.MSG_DISCONNECT)
		newFont = this.disconnectLabel;

	var timeLabel = null;
	if(this.currentMsgId == RLTMessageType.MSG_MAKE_YOU_BETS)
		timeLabel = this.timerLabel;

	var actionsHide = [];
	// считываем действия для скрытия предыдущей надписи
	this.slowHideMessageAction(actionsHide, currentFont);

	if(type == RLTMessageType.MSG_MAKE_YOU_BETS)
	{
		if(userInfo)
			this.timeMakeYourBets = userInfo;
		//_currentData = [NSDate date];
	}
	this.currentMsgId = type;

	// блок для вызова блока показа новой таблички
	var functionShowNewLabel = function(){
		newFont.stopAllActions();
		newFont.setString(this.getTextForMessage(this.currentMsgId));
		newFont.setPosition(this.getPositionForMessage(this.currentMsgId));
		var actionsShow = [];
		// считываем действия для показа новой таблички
		this.slowShowMessageAction(actionsShow, newFont);
		newFont.runAction(cc.sequence(actionsShow));
		
		if(type == RLTMessageType.MSG_MAKE_YOU_BETS)
		{
			var _actionsShow = [];
			// считываем действия для показа новой таблички
			this.slowShowMessageAction(_actionsShow, this.timerLabel);

			var doc = cc.plistParser.parse(jsb.fileUtils.getStringFromFile(res.MessagePanelDescriptionPlist));
			var dict = doc["Message"][type];
			cc.assert(dict != null, "Не найдено описание смещения таймера при сообщении 'Делайте ваши ставки'");
			var pointOffset = cc.pointFromString(dict["offset_timer_position"]);				
			this.timerLabel.stopAllActions();
			this.timerLabel.setPosition(cc.pAdd(newFont.getPosition(), pointOffset));
			this.timerLabel.setString("{0}".f(this.timeMakeYourBets));
		    this.timerLabel.runAction(cc.sequence(_actionsShow));
		    this.updateTimerLabel.call(this);
		}
	};
	
	// выполняем действия по смену надписей
	if(timeLabel)
	{
		timeLabel.stopAllActions();
		var _actionsHide = [];
		// считываем действия для скрытия предыдущей надписи
		this.slowHideMessageAction(_actionsHide, timeLabel);
		_actionsHide.push(cc.CallFunc(this.clearTimerLabel, this, null));
		timeLabel.runAction(cc.sequence(_actionsHide));
	}
	actionsHide.push(cc.CallFunc(functionShowNewLabel, this, null));
	currentFont.stopAllActions();
	newFont.stopAllActions();
	currentFont.runAction(cc.sequence(actionsHide));
};
/** @brief по идентификатору показываем сообщение
 после этого скрываем и показываем старое сообщение
 */
RLTMessagePanel.prototype.showMessageWithReturnPrevMessage = function(type) {
	
};

/** @brief мигание текущего сообщения */
RLTMessagePanel.prototype.flickingMessage = function() {
	
};

/** @brief скрывает сообщение */
RLTMessagePanel.prototype.hideMessage = function() {
	
};

