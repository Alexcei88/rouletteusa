
var RLTMODELSTATE = {};

RLTMODELSTATE.Enum(
	'MODEL_STATE_INIT',
	'MODEL_STATE_MAKE_YOUR_BETS',
	'MODEL_STATE_NO_MORE_BETS',
	'MODEL_STATE_SHOW_WIN',
	'MODEL_STATE_WAIT_WARIK',
	'MODEL_STATE_DISCONNECT'
);

var RLTMODELANSWERSETBET = {};

//возможные ответы модели на make bet
RLTMODELANSWERSETBET.Enum(
	'BET_SUCCESS',    		// ставка успешно осуществлена
	'MAX_STACK_ON_FIELD',   // на поле уже стоит максимальное количество фишек
	'LIMIT_EXCEEDED',    	// превышен лимит ставок на поле
	'TOTAL_LIMIT_EXCEEDED', // превышен общий лимит стола
	'INVALIDE_GAMESTATE',   // в этом игровом состоянии ставки делать нельзя
	'NOT_ENOUGH_CREDITS'    // не хватает денег на поддержание ставки
);

var RLTGameScene = cc.Scene.extend({
	gameTableLayer: null,
	messagePanel: null,
	historyPanel: null,
	model: null,
	ctor: function(model){
		this._super();		

		this.model = model;
		
		this.gameTableLayer = new RLTGameTableLayer(this, model);
		this.gameTableLayer.setAnchorPoint(cc.p(0, 0));
		this.addChild(this.gameTableLayer);

		cc.spriteFrameCache.addSpriteFrames(res.Panel_sheet_plist);
		
		this.messagePanel = new RLTMessagePanel();
		this.messagePanel.setAnchorPoint(cc.p(0, 0));
		this.addChild(this.messagePanel);
		
		this.historyPanel = new RLTHistoryPanel();
		this.historyPanel.setAnchorPoint(cc.p(0, 0));
		this.addChild(this.historyPanel);		
		
		this.initStateShowWin = false;
	},

	update: function(time) {
		this._super(time);
		this.gameTableLayer.update(time);
		this.messagePanel.update(time);
		this.model.Update();
	},
	
	onEnter: function()
	{
		this._super();
		this.model.StartGame();
	},
});
//----------------------------------------------------------------------------------------------------------------
RLTGameScene.prototype.modelDidInit = function(stateShowWin)
{
	cc.log("Model did init");
	this.initStateShowWin = stateShowWin;
	this.historyChanged();
	// посылаем игровому столу сообщение, что игра проинициализирована
	this.gameTableLayer.modelDidInit();
}
//----------------------------------------------------------------------------------------------------------------
// IViewDelegate
//----------------------------------------------------------------------------------------------------------------
RLTGameScene.prototype.stateChanged = function() 
{
	var currentState = this.model.GetCurrentState();
	cc.log("state Changed: %d", currentState);

	switch (currentState)
	{
		case RLTMODELSTATE.MODEL_STATE_DISCONNECT:
		{			
			this.messagePanel.showMessage(RLTMessageType.MSG_DISCONNECT, null);
			break;
		}
		case RLTMODELSTATE.MODEL_STATE_SHOW_WIN:
		{
			this.messagePanel.showMessage(RLTMessageType.MSG_SHOW_WIN, null);
			this.gameTableLayer.showWin();
			break;
		}
		case RLTMODELSTATE.MODEL_STATE_MAKE_YOUR_BETS:
		{
			this.messagePanel.showMessage(RLTMessageType.MSG_MAKE_YOU_BETS, this.model.GetRemainedTimeState());
			this.gameTableLayer.placeYourBets();
			break;
		}
		case RLTMODELSTATE.MODEL_STATE_NO_MORE_BETS:
		{
			this.messagePanel.showMessage(RLTMessageType.MSG_NO_MORE_BETS, null);
			this.gameTableLayer.noMoreBets();
			break;
		}
		case RLTMODELSTATE.MODEL_STATE_WAIT_WARIK:
		{
			this.messagePanel.showMessage(RLTMessageType.MSG_NO_MORE_BETS, null);
			this.gameTableLayer.startFallToHole();
			break;
		}
	}
}
//----------------------------------------------------------------------------------------------------------------
RLTGameScene.prototype.clearAllBets = function()
{
	this.gameTableLayer.clearAllChipsOnTable();
}
//----------------------------------------------------------------------------------------------------------------
RLTGameScene.prototype.historyChanged = function()
{
	cc.log("History changed");
	var history = this.model.GetHistory();
	this.historyPanel.addArray(history);
}
//----------------------------------------------------------------------------------------------------------------
RLTGameScene.prototype.updateCredits = function()
{

}
//----------------------------------------------------------------------------------------------------------------
RLTGameScene.prototype.didChangeBet = function(value) 
{

}
//----------------------------------------------------------------------------------------------------------------
RLTGameScene.prototype.answerDidMakeBet = function(answer)
{
	/*
//	if(answer != RLTMODELANSWERSETBET.BET_SUCCESS && answer != RLTMODELANSWERSETBET.INVALIDE_GAMESTATE)
//		[_soundWarningMakeBet play];
*/
	switch (answer)
	{
		case RLTMODELANSWERSETBET.BET_SUCCESS:
			this.gameTableLayer.didTakeBetSuccess();
			break;
	
		case RLTMODELANSWERSETBET.MAX_STACK_ON_FIELD:
			this.messagePanel.showMessageWithReturnPrevMessage(RLTMessageType.MSG_MAX_STACK_ON_FIELD);
			break;
	
		case RLTMODELANSWERSETBET.LIMIT_EXCEEDED:
		case RLTMODELANSWERSETBET.TOTAL_LIMIT_EXCEEDED:
			this.messagePanel.showMessageWithReturnPrevMessage(RLTMessageType.MSG_BETS_ARE_LIMITED);
			break;
	
		case RLTMODELANSWERSETBET.NOT_ENOUGH_CREDITS:
			this.messagePanel.showMessageWithReturnPrevMessage(RLTMessageType.MSG_NOT_ENOUGH_CREDITS);
			break;
	
		case RLTMODELANSWERSETBET.INVALIDE_GAMESTATE:
			this.messagePanel.flickingMessage();
			//[_soundWarningNoBet play];
			break;
	
		default:
			break;
	}
}
//----------------------------------------------------------------------------------------------------------------
RLTGameScene.prototype.didMakeBet = function(object) 
{
	cc.log("didMakeBet");
	return this.gameTableLayer.didMakeBet(object.indexField, object.value);
}
//----------------------------------------------------------------------------------------------------------------
RLTGameScene.prototype.didCancelBet = function(object) 
{
}

//----------------------------------------------------------------------------------------------------------------

