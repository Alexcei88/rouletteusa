var RLTKindNumber = {};
RLTKindNumber.Enum(
		"BLACK_NUMBER",
		"RED_NUMBER",
		"ZERO_NUMBER"	
);

//макс глубина хранения истории
var RLTMaxNumberHistory = 14;

var RLTHistoryPanel = cc.Node.extend({
	
	ctor: function() {
			this._super();
			
			var blackNumber = [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35];
			var redNumber   = [1, 3, 5, 7,  9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36];
			var zeroNumber  = [0, 37];			
			this.kindNumber = function(number)
			{
				if(blackNumber.indexOf(number, 0) != -1)
					return RLTKindNumber.BLACK_NUMBER;
				else if(zeroNumber.indexOf(number, 0) != -1)
					return RLTKindNumber.ZERO_NUMBER;
				else return RLTKindNumber.RED_NUMBER;
			}
			
			// панель
			this.panel = cc.Sprite.createWithSpriteFrameName("history_panel.png");
			this.panel.setPosition(cc.p(40, 310));
			this.addChild(this.panel);
			
			// черный шрифт			
			this.blackLabel = [ ];
			for(var i = 0; i < RLTMaxNumberHistory + 1; ++i)
			{
				this.blackLabel[i] = new cc.LabelBMFont("5", res.HistoryPanelBlackNumberFnt);
				this.blackLabel[i].setPosition(cc.p(100, 100));
				this.blackLabel[i].retain();
			}				
			// красный шрифт
			this.redLabel = [ ];
			for(var i = 0; i < RLTMaxNumberHistory + 1; ++i)
			{
				this.redLabel[i] = new cc.LabelBMFont("5", res.HistoryPanelRedNumberFnt);
				this.redLabel[i].setPosition(cc.p(100, 100));
				this.redLabel[i].retain();
			}
			
			// зеленый шрифт
			this.zeroLabel = [ ];
			for(var i = 0; i < RLTMaxNumberHistory + 1; ++i)
			{
				this.zeroLabel[i] = new cc.LabelBMFont("0", res.HistoryPanelZeroNumberFnt);
				this.zeroLabel[i].setPosition(cc.p(100, 100));
				this.zeroLabel[i].retain();
			}			
			this.currentNumbers = [];		
			
			this.pointStartNumber = cc.p(40, 32);
			this.YDeltaNumber = 29;
			return true;
			
			// необходимо вызывать в деструкторе
			this.clearLabel = function() {
				for(var i = 0; i < RLTMaxNumberHistory + 1; ++i)
					this.blackLabel[i].release();
				// красный шрифт
				for(var i = 0; i < RLTMaxNumberHistory + 1; ++i)
					this.redLabel[i].release();

				// зеленый шрифт
				for(var i = 0; i < RLTMaxNumberHistory + 1; ++i)
					this.zeroLabel[i].release();
			}
	}
});
//----------------------------------------------------------------------------------------------------------------
RLTHistoryPanel.prototype.addNumber = function(number) 
{	
	var kind = this.kindNumber(number);
	var currentFont = null;
	
	function findFreeFont(labelArray)
	{
		for(var i = 0; i < RLTMaxNumberHistory + 1; ++i)
		{
			if(labelArray[i].parent == null)
			{
				currentFont = labelArray[i];
				break;
			}
		}
	};
	
	switch (kind)
	{
		case RLTKindNumber.BLACK_NUMBER:
			findFreeFont.call(this, this.blackLabel);
			break;
		case RLTKindNumber.RED_NUMBER:
			findFreeFont.call(this,this.redLabel);
			break;
		case RLTKindNumber.ZERO_NUMBER:
			findFreeFont.call(this, this.zeroLabel);
			break;
	}

	cc.assert(currentFont != null, "Нет шрифта для новой цифры в истории");
	// начинаем добавление
	var arrayChildPanel = this.panel.children;	
	var pos = arrayChildPanel.length;
	if(pos == RLTMaxNumberHistory)
	{		
		var tempArray = [number];
		for(var i = 1; i < RLTMaxNumberHistory; ++i)
		{
			tempArray[i] = this.currentNumbers[i - 1];
		}

		this.currentNumbers = tempArray;
		// удаляем первый
		this.panel.removeChild(arrayChildPanel[0]);
		
		var y = this.pointStartNumber.y + (RLTMaxNumberHistory - 1) * this.YDeltaNumber;
		currentFont.setPosition(cc.p(this.pointStartNumber.x, y));
		
		// сдвигаем все остальные
		for(var i = 0; i < RLTMaxNumberHistory - 1; ++i)
		{
			var font = arrayChildPanel[i];
			font.setPosition(cc.p(this.pointStartNumber.x, this.pointStartNumber.y + i * this.YDeltaNumber));
		}
	}
	else
	{
		this.currentNumbers[RLTMaxNumberHistory - pos - 1] = number;
		currentFont.setPosition(cc.p(this.pointStartNumber.x, this.pointStartNumber.y + pos * this.YDeltaNumber));
	}

	if(number == 37)
		currentFont.setString("00");
	else
		currentFont.setString(number.toString());

	this.panel.addChild(currentFont);
};

RLTHistoryPanel.prototype.addArray = function (numbers) {
	
	if(numbers.length >= RLTMaxNumberHistory)
	{
		// если в новом массиве все те номера, которые уже отображаются
		// то нечего делать
		var tmpArrayHistory = new Array(numbers);
		tmpArrayHistory.length = RLTMaxNumberHistory;
		if(tmpArrayHistory.equals(this.currentNumbers))
			return;
		
		cc.log("!!! FULL UPDATE HISTORY PANEL");

		// будет полностью обновление всех элементов
		this.panel.removeAllChildren(false);
		for(var i = numbers.length - 2; i > 0; --i)
			this.addNumber(+numbers[i]);
	}	
	else
	{
		for(var i = numbers.length - 2; i >= 0; --i)
			this.addNumber(+numbers[i]);
	}
};