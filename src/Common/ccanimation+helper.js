

cc.Animation.createAnimation = function(nameFormat, frameCount, delay) {

	var frames = [];
	for(var i = 0; i < frameCount; ++i)
	{
		var frameName = nameFormat.f(i);
		var frame = cc.spriteFrameCache.getSpriteFrame(frameName);
		cc.assert(frame != null, "Не найден спрайтовый фрейм в кэше");
		frames.push(frame);
	}	
	return cc.Animation.create(frames, delay, 1);
};

/*
+(instancetype) animationWithFrameNameFormat:(NSString*)nameFormat
frameCount:(NSUInteger)frameCount
delay:(float)delay;

+(instancetype)animationWithFrameNameFormat:(NSString *)nameFormat
frameCount:(NSUInteger)frameCount
delay:(float)delay
reverse:(BOOL)reverse;

+(instancetype) animationWithFrameNameFormat:(NSString*)nameFormat
frameCount:(NSUInteger)frameCount
delay:(float)delay
startFrame:(NSUInteger)startFrame
includeBeginFrames:(BOOL)includeBeginFrames;

+ (instancetype)animationWithFrameNameFormat:(NSString*)nameFormat
numbers:(NSArray*)numbers
delay:(float)delay;

//получение анимации для символа - вначале из кэша если нет, то создание и помещение в кэш
+(instancetype) animationWithFrameNameFormat:(NSString*)nameFormat
animationName:(NSString*)nameAnimation
frameCount:(NSUInteger)frameCount
delay:(float)delay;

/**
@param nameFormat - name format, for example (animation_frame_%03d.png)
@param frameCount - frame count
@param duration - desired duration of animation. Delay will be calculated
@return animation
 */
