//Production steps of ECMA-262, Edition 5, 15.4.4.14
//Reference: http://es5.github.io/#x15.4.4.14
if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function(searchElement, fromIndex) {

		var k;

		// 1. Let O be the result of calling ToObject passing
		//    the this value as the argument.
		if (this == null) {
			throw new TypeError('"this" is null or not defined');
		}

		var O = Object(this);

		// 2. Let lenValue be the result of calling the Get
		//    internal method of O with the argument "length".
		// 3. Let len be ToUint32(lenValue).
		var len = O.length >>> 0;

		// 4. If len is 0, return -1.
		if (len === 0) {
			return -1;
		}

		// 5. If argument fromIndex was passed let n be
		//    ToInteger(fromIndex); else let n be 0.
		var n = +fromIndex || 0;

		if (Math.abs(n) === Infinity) {
			n = 0;
		}

		// 6. If n >= len, return -1.
		if (n >= len) {
			return -1;
		}

		// 7. If n >= 0, then Let k be n.
		// 8. Else, n<0, Let k be len - abs(n).
		//    If k is less than 0, then let k be 0.
		k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

		// 9. Repeat, while k < len
		while (k < len) {
			// a. Let Pk be ToString(k).
			//   This is implicit for LHS operands of the in operator
			// b. Let kPresent be the result of calling the
			//    HasProperty internal method of O with argument Pk.
			//   This step can be combined with c
			// c. If kPresent is true, then
			//    i.  Let elementK be the result of calling the Get
			//        internal method of O with the argument ToString(k).
			//   ii.  Let same be the result of applying the
			//        Strict Equality Comparison Algorithm to
			//        searchElement and elementK.
			//  iii.  If same is true, return k.
			if (k in O && O[k] === searchElement) {
				return k;
			}
			k++;
		}
		return -1;
	};
}

Array.prototype.equals = function(object2) {
	//For the first loop, we only check for types
	for (propName in this) {
		//Check for inherited methods and properties - like .equals itself
		//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/hasOwnProperty
		//Return false if the return value is different
		if (this.hasOwnProperty(propName) != object2.hasOwnProperty(propName)) {
			return false;
		}
		//Check instance type
		else if (typeof this[propName] != typeof object2[propName]) {
			//Different types => not equal
			return false;
		}
	}
	//Now a deeper check using other objects property names
	for(propName in object2) {
		//We must check instances anyway, there may be a property that only exists in object2
		//I wonder, if remembering the checked values from the first loop would be faster or not 
		if (this.hasOwnProperty(propName) != object2.hasOwnProperty(propName)) {
			return false;
		}
		else if (typeof this[propName] != typeof object2[propName]) {
			return false;
		}
		//If the property is inherited, do not check any more (it must be equa if both objects inherit it)
		if(!this.hasOwnProperty(propName))
			continue;

		//Now the detail check and recursion

		//This returns the script back to the array comparing
		/**REQUIRES Array.equals**/
		if (this[propName] instanceof Array && object2[propName] instanceof Array) {
			// recurse into the nested arrays
			if (!this[propName].equals(object2[propName]))
				return false;
		}
		else if (this[propName] instanceof Object && object2[propName] instanceof Object) {
			// recurse into another objects
			//console.log("Recursing to compare ", this[propName],"with",object2[propName], " both named \""+propName+"\"");
			if (!this[propName].equals(object2[propName]))
				return false;
		}
		//Normal value comparison for strings and numbers
		else if(this[propName] != object2[propName]) {
			return false;
		}
	}
	//If everything passed, let's say YES
	return true;
}  