var LoaderScene = cc.Scene.extend({
	ctor: function(loaderScreen, loaderBar, g_resource){
		
		this._super();
		var sprite = new cc.Sprite(loaderScreen);
		sprite.setAnchorPoint(cc.p(0, 0));
		this.addChild(sprite);
		
		var loadBar = new cc.Sprite(loaderBar);
		loadBar.retain();
		loadBar.setAnchorPoint(cc.p(0, 0));
		loadBar.setPosition(cc.p(238, 17));
		
		var loadBarNormalWidth = loadBar.getContentSize().width;
		var rect = loadBar.getTextureRect();
		rect.width = 0;
		loadBar.setTextureRect(rect);
		this.addChild(loadBar);

		var allLoadedCount = g_resource.length;
		var countLoaded = 0;
		
		this.loadedNextElement = function() 
		{
			countLoaded++;
			var procent = countLoaded/allLoadedCount;
			var newRect = loadBar.getTextureRect();
			newRect.width = procent * loadBarNormalWidth;
			loadBar.setTextureRect(newRect);

			cc.director.drawScene();
		}
		return true;
	}
});
