#pragma once

#include <string>
#include <vector>
#include <map>

#include "RLTFakeClient.h"

#include "jsapi.h"
#include "jsfriendapi.h"

class SHTimer;
class RLTModel
{
public:
	enum RLTMODELSTATE 
	{
		MODEL_STATE_INIT = 0,
		MODEL_STATE_MAKE_YOUR_BETS = 1,
		MODEL_STATE_NO_MORE_BETS = 2,
		MODEL_STATE_SHOW_WIN = 3,
		MODEL_STATE_WAIT_WARIK = 4,
		MODEL_STATE_DISCONNECT = 5		
	};

	enum RLTTIMEWAIT 
	{
		TIME_WAIT_FALLINHOLE  = 0, // ����� �������� ������� ������ � �����
	    TIME_WAIT_SHOW_WIN    = 1 // ����� ������ ��������
	};
		
	// ��������� ������ ������ �� make bet
	enum RLT_ANSWERMODEL_MAKEBET
	{
		BET_SUCCESS             = 0,    // ������ ������� ������������
		MAX_STACK_ON_FIELD      = 1,    // �� ���� ��� ����� ������������ ���������� �����
		LIMIT_EXCEEDED          = 2,    // �������� ����� ������ �� ����
		TOTAL_LIMIT_EXCEEDED    = 3,    // �������� ����� ����� �����
		INVALIDE_GAMESTATE      = 4,    // � ���� ������� ��������� ������ ������ ������
		NOT_ENOUGH_CREDITS      = 5,    // �� ������� ����� �� ����������� ������
	};

	RLTModel(void);
	~RLTModel(void);

	void SetDelegate(JSObject* delegate) { this->viewDelegate = delegate; }

	std::string getString()
	{
		std::string str = "testString!!!";
		return str;
	}
	// ������ ����
	void StartGame();
	// ��������� ����
	void StopGame();

	// ���������� ������� � ������
	void Update();

	// �������
	// ����� ������� ���������
	RLTMODELSTATE GetCurrentState() { return currentState; }
	// ��������� ����� ���������� � ������� ���������
	float GetRemainedTimeState() {return fakeClient->GetRemainedTime(); }
	// ��������� ������
	unsigned int GetTotalBet() { return totalBet; }
	// �������� ��������
	unsigned int GetCredits() { return credits;}
	std::vector<int> GetHistory() { return fakeClient->GetHistory(); }
	// ������� � ��������
	unsigned int GetWin() { return win; }
	// ����� ����������� ������
	unsigned int GetWinNumber() { return fakeClient->GetWinNumber(); }
	const std::vector<int> GetNominalsChip() { return fakeClient->GetNominals(); }

	const std::vector<int> GetLosingFields() { return losingFields; }
	const std::vector<int> GetWinningFields() { return winningFields; }

	/** @brief ����� ���� � ����� */
	void DidFallInHole();

	/** @brief ������� ������� ��������� */
	void DidEndShowWin();

	/** @brief ������������ ����� ����������� ������ */
	void RestoreBets();

	/** @brief ������� ������ �� ������� ���� */
	void SetBet(const unsigned int& indexField, const unsigned int& value);

	/** @brief ������� ������ �� ������� ���� */
	void RemoveBet(const unsigned int& indexField, const unsigned int& value);

	/** @brief ���� �������� ������ ������ */
	void LimitsObtained();

	/** @brief ���� �������� �������� */
	void NominalsObtained();

	/** @brief ���� �������� ������� */
	void HistoryObtained();

	/** @brief �������� ��������� ������� */
	void StateObtained();

	/** @brief ����� ������ ���� ������� */
	void SessionOpened();

/** @brief ������ ���� ���������� �� ������ 
    @param success - ������, ��������
    @parama errorString - ��������� �� ������
 */
	void BetSended(bool succes, const std::string error);

	/** @brief ��� ����� � �������� */
	void DisconnectWithError(const std::string error);

	void InitField(const unsigned int& index, const std::vector<int>& arrayBets);


private:
	enum RLT_TASKFORCLIENT
	{
		NONE_TASK             = 0,
		CONFIG_TASK           = 1 << 0,
		HISTORY_TASK          = 1 << 1,
		OPEN_SESSION          = 1 << 2,
		GETSTATE_TASK         = 1 << 3
	};

	// ��������� ��� �������� ���� ��� ������� ������
	typedef struct
	{
		// ����������� ������
		unsigned int coeff;
		// ������ �����, ���� ������ ������
		std::vector<int> numbersMakeBets;
		// ����� �������
		unsigned int length;
		// ������� ������
		unsigned int bet;    
	} RLT_BetsOnZone;

	// ������� ���������� ������
	static const unsigned int BASE_COEFF_BET = 36;
	//����, �������� �������� ������ ������
	static const unsigned int kTOTAL_LIMIT = 36;
	// ������� �������� ��������� ������� ������
	static const unsigned int kMAXCOUNT_HISTORY_BETS = 12;

	RLTFakeClient* fakeClient;
	JSObject* viewDelegate;

	// ���������� ������� �����, ���� ����� ����� ������� ������
	static const int NUMBER_OF_FIELDS = 161;
	// ����� ���� �� ������� ����������(�� ���������� ������ five)
	static const int NUMBER_FIELD_FIELD = 3;
	// ������� ���������
	RLTMODELSTATE currentState;
	// ��������� ���������
	RLTMODELSTATE lastState;

	// ��������� ����� ���������� � ������� ���������
    float remainedTimeState;
	// ��������� ������
	unsigned int totalBet;
	// ��������� �������� ������
	unsigned int lastTotalBet;
	// �������� ��������
    unsigned int credits;
	// ������� � ��������
	unsigned int win;
	// ������, ������� ���������� ���������
	unsigned int tasks;
    // ������, ������������ �� ������
	std::vector<int> betsToServer;
	 // ��������� �������� ��������� ������
	bool    lastBetsSended;

	// ���������� � ����� ����� ��� ������� ������
    std::vector<RLT_BetsOnZone> betsOnZone;
	// ��������� ����������� ������ �� ����
    std::vector<int> lastBetsOnZone;
    // ������� ��������� ��������� ������ �� ����
    std::vector<RLT_BetsOnZone> historyBetsValueOnZone;
    // ������� ��������� ��������� ������ �� ����
    std::vector<RLT_BetsOnZone> historyIndexFieldOnZone;

	// ������ �������� ���������� �����
	std::vector<int> winningFields;
	// ������ �������� ����������� �����
	std::vector<int> losingFields;

	// �����, ���������� ��� ������������� �������
    bool    nominalsObtained;
    bool    limitsObtained;
    bool    historyObtained;

	// ����� �������������� �����
	unsigned int numberSpin;

	// ����� ���� � �����
    bool    fallInHole;
    // ���������� ����� ��������
    bool    finishedShowWin;

	// ����� �������� � ����� ���������
	uint64_t timeChangeState;
	// ����� �������� �������
	bool needUpdateCredits;

	// ���������� �������
	void ContinuePolling();

	void PollClient();
	// �������� ��������� ������    
	void UpdateState(const RLTMODELSTATE& newStateModel);
	
	void ResetBets();
	void SavedBets();

	void DeferredRestartPolling(const unsigned long remainedTime);
	void UpdateStateClient();

	bool IsChangedStateClient();
	bool IsBreakSequenceState(RLTFakeClient::RLTCLIENTSTATE currentClientState);

	// ������ ��� ���������� ��������� �������
	SHTimer* timerUpdateState;

	SHTimer* timerCheckFallInHole;
	void CheckFallInHole();

	std::map<int, float> timeSpentState;

	bool IsEnoughLimit(const unsigned int& field, const unsigned int& value) const;
	bool IsEnoughCommonLimit(const unsigned int& value) const;

	void MakeBet(const unsigned int& field, const unsigned int& value);
	void UnsetBet(const unsigned int& field, const unsigned int& value);

	void CalculateWinningAndLossingFields();
	/** @brief �������� �� ������ ������ */
//-(BOOL) enableCancelBet;
/** @brief ��������� ������������������ ���������(����� ��� ����� �� ���� � ����� � ��������)*/
//-(BOOL) breakSequenceStateFromLobby;

};
