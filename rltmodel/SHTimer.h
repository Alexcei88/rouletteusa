#pragma once
#include <functional>

class SHTimer
{
public:
	SHTimer(void);
	~SHTimer(void);

	void Update();
	// periodTime(���) - �����, ����� ������� ����� ������� �������
	void SetTimer(const unsigned long long& periodTime, std::function<void ()> callback);
	void ResetTimer();

	static unsigned long long GetTime();
	static unsigned long long GetPeriodTime(const unsigned long long& startTime);

private:
	// ����� ������ ������������ �������
	unsigned long long startTime;
	// ������� ������� ����� �����
	unsigned long long periodTime;
	// ������� ��� ������ �� �������
	std::function<void()> callback;
};


