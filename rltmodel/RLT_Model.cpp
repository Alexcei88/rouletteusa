#include "RLT_Model.h"
#include "ScriptingCore.h"
#include "SHTimer.h"
#include "jstypes.h"

//------------------------------------------------------------------------------------
RLTModel::RLTModel(void): 
	 viewDelegate(nullptr)
	,tasks(0)
    ,currentState(MODEL_STATE_INIT)
    ,lastState(MODEL_STATE_INIT)        
    ,nominalsObtained(false)
    ,limitsObtained(false)
    ,historyObtained(false)
	,fallInHole(false)
	,finishedShowWin(false)
	,needUpdateCredits(false)
	,totalBet(0)
{
	fakeClient = new RLTFakeClient(this);
	timerUpdateState = new SHTimer();
	timerCheckFallInHole = new SHTimer();
	betsToServer.resize(38);	
	betsOnZone.resize(NUMBER_OF_FIELDS);
	lastBetsOnZone.resize(NUMBER_OF_FIELDS);

	timeSpentState[TIME_WAIT_FALLINHOLE] = 15.0f;
	timeSpentState[TIME_WAIT_SHOW_WIN] = 2.0f;
}
//------------------------------------------------------------------------------------
RLTModel::~RLTModel(void)
{
	delete fakeClient;
	delete timerUpdateState;
	delete timerCheckFallInHole;
}
//------------------------------------------------------------------------------------
void RLTModel::StartGame()
{
	 // ����� �������� �������, ������ � ������� ���������
    tasks = tasks | CONFIG_TASK | HISTORY_TASK | GETSTATE_TASK;    
    // ��������� �������
    this->ContinuePolling();
}
//------------------------------------------------------------------------------------
void RLTModel::StopGame()
{
	tasks = 0;
	timerUpdateState->ResetTimer();
	timerCheckFallInHole->ResetTimer();
}
//------------------------------------------------------------------------------------
void RLTModel::Update()
{
	timerUpdateState->Update();
	timerCheckFallInHole->Update();
}
//------------------------------------------------------------------------------------
void RLTModel::LimitsObtained()
{
	tasks = tasks & ~CONFIG_TASK;
    limitsObtained = true;
}
//------------------------------------------------------------------------------------
void RLTModel::NominalsObtained()
{
	tasks = tasks & ~CONFIG_TASK;
    nominalsObtained = true;
	this->ContinuePolling();
}
//------------------------------------------------------------------------------------
void RLTModel::HistoryObtained()
{
	printf("History obtained!!!");
	tasks = tasks & ~HISTORY_TASK;
    historyObtained = true;
	if(currentState != MODEL_STATE_INIT)
	{
		if(viewDelegate)
			ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(viewDelegate), "historyChanged", 0, NULL, NULL);
	}

	this->ContinuePolling();	
}
//------------------------------------------------------------------------------------
void RLTModel::StateObtained()
{
	// ������ ��� �� �������������������
    if(lastState == MODEL_STATE_INIT)
    {
        if(!historyObtained)
            // ��� �������, ���� ������� �� ��������� �������
            tasks = tasks | HISTORY_TASK;
        if(!limitsObtained || !nominalsObtained)
            // ��� ������� ��� ���������, ���� ������� �� ��������� �������
            tasks = tasks | CONFIG_TASK;
        
        // ���� ��������� �� ��, ������� ��� �����, �� ���������� ����������
        if(!historyObtained || !limitsObtained ||
           !nominalsObtained)
        {
            // ���� ������� �� ��������� ���������
            tasks = tasks | GETSTATE_TASK;
			this->ContinuePolling();
        }
        else
        {
            // ������ �������������������
            tasks = tasks & ~GETSTATE_TASK;
            //[self checkNominals];
			credits = fakeClient->GetCredits();
			const RLTFakeClient::RLTCLIENTSTATE clientState = fakeClient->GetState();
			printf("INIT: state: %d \n", clientState);
			const bool showWin = clientState == RLTFakeClient::STATE_SHOW_WIN;
			jsval arg = BOOLEAN_TO_JSVAL(showWin);        
			if(viewDelegate)
				ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(viewDelegate), "modelDidInit", 1, &arg, NULL);

            switch (clientState)
            {
                case RLTFakeClient::STATE_MAKE_YOUR_BETS:
					this->UpdateState(MODEL_STATE_MAKE_YOUR_BETS);
                    break;
                case RLTFakeClient::STATE_SHOW_WIN:
                    fallInHole = true;
                    finishedShowWin = false;
					this->UpdateState(MODEL_STATE_SHOW_WIN);
                    break;
                case RLTFakeClient::STATE_NO_MORE_BETS:
					this->UpdateState(MODEL_STATE_NO_MORE_BETS);
                    break;
                default:
                    break;
            }
            //  ����� ������������ ������������������ ���������� ���� ��������� ������������������
			this->DeferredRestartPolling(fakeClient->GetRemainedTime());
        }
    }
    else
    {
        // ������� ����� ������
		if(this->IsChangedStateClient())
        {
            // ��������� ����������, ������� ����������
            tasks = tasks & ~GETSTATE_TASK;
            this->ContinuePolling();
			this->DeferredRestartPolling(fakeClient->GetRemainedTime());
			const RLTFakeClient::RLTCLIENTSTATE clientState = fakeClient->GetState();
			const bool breakSequenceState = this->IsBreakSequenceState(clientState);
            if(breakSequenceState)
            {
                // ���������� ������
                this->ResetBets();
                // ������� ���� �� �����
				if(viewDelegate)
					ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(viewDelegate), "clearAllBets", 1, NULL, NULL);
                needUpdateCredits = true;
            }
            switch (clientState)
            {
                case RLTFakeClient::STATE_MAKE_YOUR_BETS:
                {
                    if((fallInHole && finishedShowWin) || breakSequenceState)
                    {
                        // ����� � ����� � ������� �������
                        // ��� ��������� ��������� ������������������ ���������
						this->UpdateState(MODEL_STATE_MAKE_YOUR_BETS);
                    }
                    break;
                }
                case RLTFakeClient::STATE_NO_MORE_BETS:
						this->UpdateState(MODEL_STATE_NO_MORE_BETS);
                    break;
                    
                case RLTFakeClient::STATE_SHOW_WIN:
                    if(!breakSequenceState)
                    {
                        // ��������� ������ � ��������� �������� ������� ������ � �����
						this->UpdateState(MODEL_STATE_WAIT_WARIK);
                    }
                    else
                    {
                        // ����� �� �������������, ��� ����� � �����
                        currentState = MODEL_STATE_WAIT_WARIK;
                        //[self didFallInHole];
                    }
                    break;
                    
                default:
					this->UpdateState(MODEL_STATE_DISCONNECT);
                    break;
            }
        }
        else
        {
            // ������ ������ ������� �������
			this->ContinuePolling();
        }
    }
    if(needUpdateCredits)
    {
        needUpdateCredits = true;
		if(viewDelegate)
			ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(viewDelegate), "updateCredits", 1, NULL, NULL);
    }
}
//------------------------------------------------------------------------------------
void RLTModel::SessionOpened()
{

}
//------------------------------------------------------------------------------------
void RLTModel::BetSended(bool succes, const std::string error)
{

}
//------------------------------------------------------------------------------------
void RLTModel::DisconnectWithError(const std::string error)
{

}
//-------------------------------------------------------------------------------------------------------------
void RLTModel::InitField(const unsigned int& index, const std::vector<int>& arrayBets)
{
	RLT_BetsOnZone zone;
    zone.length = arrayBets.size();
    if(index == NUMBER_FIELD_FIELD)
        zone.coeff = 7;
    else
        zone.coeff = BASE_COEFF_BET/zone.length;
    
	zone.numbersMakeBets.assign(arrayBets.begin(), arrayBets.end());
	zone.bet = 0;
	betsOnZone.at(index) = zone;
}
//------------------------------------------------------------------------------------	
void RLTModel::ContinuePolling()
{
    if(tasks != 0)
        this->PollClient();
}
//------------------------------------------------------------------------------------	
void RLTModel::PollClient()
{
	_ASSERT(tasks != 0);
    
	printf("Poll client\n");
    // ����� ��������� ����� ������� ������
	unsigned int idTask = 0;
    unsigned int pos = 0;
    while(!idTask)
    {
        idTask = tasks & (1 << pos);
        pos++;
    }
    RLT_TASKFORCLIENT currentTask = (RLT_TASKFORCLIENT)idTask;
    switch (currentTask) 
	{
        case CONFIG_TASK:
            fakeClient->GetConfig();
            break;
        case HISTORY_TASK:
            fakeClient->UpdateHistory();
            break;
            
        case OPEN_SESSION:
            //fakeClient->OpenSession();
            break;
            
        case GETSTATE_TASK:
            fakeClient->UpdateState();
            break;            
        default:
            break;
    }
}
//-------------------------------------------------------------------------------------------------------------
void RLTModel::ResetBets()
{
	totalBet = 0;
    for(int i = 0; i < NUMBER_OF_FIELDS; ++i)
        betsOnZone[i].bet = 0;
    for(int i = 0; i < betsToServer.size(); ++i)
        betsToServer[i] = 0;
    
	fallInHole = false;
    /*
    [_historyIndexFieldOnZone removeAllObjects];
    [_historyBetsValueOnZone removeAllObjects];*/
}
//-------------------------------------------------------------------------------------------------------------
void RLTModel::SavedBets()
{
    for(int i = 0; i < NUMBER_OF_FIELDS; ++i)
    {
        lastBetsOnZone[i] = betsOnZone[i].bet;
    }
    lastTotalBet = totalBet;
}
//-------------------------------------------------------------------------------------------------------------
// �����������, ����� ����� ����� �� ����� ��������� ���������� ������� ��� ������ ��������� �������
void RLTModel::DeferredRestartPolling(const unsigned long remainedTime)
{
	timerUpdateState->SetTimer(remainedTime, std::bind(&RLTModel::UpdateStateClient, this));
}
//-------------------------------------------------------------------------------------------------------------
void RLTModel::UpdateStateClient()
{
	timerUpdateState->ResetTimer();	
	tasks = tasks | GETSTATE_TASK;
	this->ContinuePolling();
}
//------------------------------------------------------------------------------------	
bool RLTModel::IsChangedStateClient()
{
	RLTFakeClient::RLTCLIENTSTATE stateClient = fakeClient->GetState();
    bool didChanged = false;

	printf("IsChangedStateClient: currentStateClient = %d\n", stateClient);
	switch (stateClient)
    {
        case RLTFakeClient::STATE_MAKE_YOUR_BETS:
            if(lastState != MODEL_STATE_MAKE_YOUR_BETS)
                didChanged = true;
            break;
        
        case RLTFakeClient::STATE_NO_MORE_BETS:
            if(lastState != MODEL_STATE_NO_MORE_BETS)
                didChanged = true;
            break;
        case RLTFakeClient::STATE_SHOW_WIN:
            if(lastState != MODEL_STATE_SHOW_WIN && currentState != MODEL_STATE_WAIT_WARIK)
                didChanged = true;
            break;
        case RLTFakeClient::STATE_UNKNOWN:
			if(currentState != MODEL_STATE_DISCONNECT)
                didChanged = true;
        default:
            break;
    }
    return didChanged;
}
//------------------------------------------------------------------------------------	
bool RLTModel::IsBreakSequenceState(RLTFakeClient::RLTCLIENTSTATE currentClientState)
{
    switch (currentClientState)
    {
        case RLTFakeClient::STATE_MAKE_YOUR_BETS:
			if((lastState != MODEL_STATE_WAIT_WARIK && lastState != MODEL_STATE_SHOW_WIN) || (fakeClient->GetNumberSpin() - numberSpin != 1))
                return true;
            break;
            
        case RLTFakeClient::STATE_SHOW_WIN:
            if(lastState != MODEL_STATE_NO_MORE_BETS
               || numberSpin != fakeClient->GetNumberSpin())
                return true;
            break;
            
        case RLTFakeClient::STATE_NO_MORE_BETS:
            if(lastState != MODEL_STATE_MAKE_YOUR_BETS || numberSpin != fakeClient->GetNumberSpin())
                return true;
            break;
        default:
            break;
    }
    return false;
}
//------------------------------------------------------------------------------------	
void RLTModel::UpdateState(const RLTMODELSTATE& newStateModel)
{
	printf("!UpdateState: %d\n", newStateModel);
	assert(lastState != newStateModel);
    
	credits = fakeClient->GetCredits();
	currentState = newStateModel;
	// ���������� ������� ����� ��������  
	timeChangeState = SHTimer::GetTime();
	
	switch (newStateModel)
    {
        case MODEL_STATE_MAKE_YOUR_BETS:
			this->ResetBets();
			this->numberSpin = fakeClient->GetNumberSpin();
			// ���� ������� �� ���������� �������
            tasks = tasks | HISTORY_TASK;
            this->ContinuePolling();
            break;
            
        case MODEL_STATE_NO_MORE_BETS:
        {
#ifdef DEBUG
            NSUInteger totalBet = 0;
            for(NSUInteger i = 0; i < kNUMBER_OF_FIELDS; ++i)
            {
                RLT_BetsOnZone* betsOnZone = &_betsOnZone[i];
                totalBet += betsOnZone->bet;
            }
            NSAssert(totalBet == _totalBet, @"��������� ������ ������� c�����, ������������� �� ������, �� ����� ��������� ������ ���� ������");
#endif
            // ������ ���������� ���������, �� ����� ������ �����
			fakeClient->SendBets(betsToServer);
			if(totalBet > 0)
				this->SavedBets();
            break;
        }
            
        case MODEL_STATE_SHOW_WIN:
			this->CalculateWinningAndLossingFields();
            break;
            
        case MODEL_STATE_WAIT_WARIK:

            // ����� ����� ��������
            // ����� �����, ������� ���� �� ��������� �����, ���������, ��������� �� �����
			timerCheckFallInHole->ResetTimer();
			timerCheckFallInHole->SetTimer(this->timeSpentState[TIME_WAIT_FALLINHOLE], std::bind(&RLTModel::CheckFallInHole, this));
            break;
            
        case MODEL_STATE_DISCONNECT:
            break;
            
        default:
            break;
    }
    // ���������� ��������� �������� ���������
	if(newStateModel != MODEL_STATE_DISCONNECT)
		lastState = newStateModel;
    
    // �������� �������� ������ � ����� ������ ���������
	if(viewDelegate)
		ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(viewDelegate), "stateChanged", 0, NULL, NULL);
}
//-------------------------------------------------------------------------------------------------------------
void RLTModel::DidFallInHole()
{
	assert(this->currentState == MODEL_STATE_WAIT_WARIK || this->currentState == MODEL_STATE_SHOW_WIN);
	timerCheckFallInHole->ResetTimer();
    
    this->fallInHole = true;
    this->finishedShowWin = false;
    // ��������� ������ � ��������� ������ ��������(���� ��� ��� � ��������� ������� ������)
	if(this->currentState != MODEL_STATE_SHOW_WIN)
        this->UpdateState(MODEL_STATE_SHOW_WIN);
}
//-------------------------------------------------------------------------------------------------------------
void RLTModel::DidEndShowWin()
{
    printf("Did end show win!\n");
	if(this->currentState != MODEL_STATE_SHOW_WIN)
        return;
    
    finishedShowWin = true;
    // ���� ������ � ��������� ������� ���� ������, � ������ ���, �� ��������� ������ � ������ ���������
	if(fakeClient->GetState() == RLTFakeClient::STATE_MAKE_YOUR_BETS && this->currentState != MODEL_STATE_MAKE_YOUR_BETS)
        this->UpdateState(MODEL_STATE_MAKE_YOUR_BETS);
}
//-------------------------------------------------------------------------------------------------------------
void RLTModel::SetBet(const unsigned int& indexField, const unsigned int& value)
{
	// 1. �������� �� ���������
	if(this->currentState != MODEL_STATE_MAKE_YOUR_BETS)
    {
        //[self.modelDelegate answerDidMakeBetOnField:INVALIDE_GAMESTATE];
		if(viewDelegate)
		{
			jsval arg = INT_TO_JSVAL(INVALIDE_GAMESTATE);        
			ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(viewDelegate), "answerDidMakeBet", 1, &arg, NULL);
		}
        printf("unsuccess make bet on field: %d. Reason: %d", indexField, INVALIDE_GAMESTATE);
        return;
    }

	// 2. �������� �� �������
	if((this->GetCredits() - totalBet - value) < 0 || (value == -1 && this->GetCredits() - totalBet <= 0))
    {
		if(viewDelegate)
		{
			jsval arg = INT_TO_JSVAL(NOT_ENOUGH_CREDITS);        
			ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(viewDelegate), "answerDidMakeBet", 1, &arg, NULL);
		}
        printf("unsuccess make bet on field: %d. Reason: %d", indexField, NOT_ENOUGH_CREDITS);
        return;
    }

	// 3. �������� �� ����� �� ����
	if(!this->IsEnoughLimit(indexField, value))
    {
        if(viewDelegate)
		{
			jsval arg = INT_TO_JSVAL(LIMIT_EXCEEDED);        
			ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(viewDelegate), "answerDidMakeBet", 1, &arg, NULL);
		}
		printf("unsuccess make bet on field: %d. Reason: %d", indexField, LIMIT_EXCEEDED);
        return;
    }

	// 4.  �������� ������ ������ �� ����
	if(!this->IsEnoughCommonLimit(value))
    {
        if(viewDelegate)
		{
			jsval arg = INT_TO_JSVAL(LIMIT_EXCEEDED);        
			ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(viewDelegate), "answerDidMakeBet", 1, &arg, NULL);
		}
		printf("unsuccess make bet on field: %d. Reason: %d", indexField, LIMIT_EXCEEDED);
        return;
    }

	// ����� ������� ������ ������ �� ����
	if(viewDelegate)
	{
		bool result = false;
		
		ScriptingCore* core = ScriptingCore::getInstance();
		JSObject* object = JS_NewObject(core->getGlobalContext(), NULL, NULL, NULL); 
		JS_DefineProperty(core->getGlobalContext(), object, "indexField", INT_TO_JSVAL(indexField), NULL, NULL, JSPROP_READONLY);
		JS_DefineProperty(core->getGlobalContext(), object, "value", INT_TO_JSVAL(value), NULL, NULL, JSPROP_READONLY);

		jsval retval;
		jsval arg = OBJECT_TO_JSVAL(object);

		core->executeFunctionWithOwner(OBJECT_TO_JSVAL(viewDelegate), "didMakeBet", 1, &arg, &retval);			
		result = JSVAL_TO_BOOLEAN(retval);

		if(result)
		{
			this->MakeBet(indexField, value);

			jsval arg = INT_TO_JSVAL(BET_SUCCESS);        
			ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(viewDelegate), "answerDidMakeBet", 1, &arg, NULL);
			printf("success make bet on field: %d", indexField);

			//[self addHistoryBetsOnField:indexField value:value];
		}
		else
		{
			jsval arg = INT_TO_JSVAL(MAX_STACK_ON_FIELD);        
			ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(viewDelegate), "answerDidMakeBet", 1, &arg, NULL);
		
			printf("unsuccess make bet on field: %d. Reason: %d", indexField, MAX_STACK_ON_FIELD);
		}	
	}
}
//-------------------------------------------------------------------------------------------------------------
void RLTModel::RestoreBets()
{

}
//-------------------------------------------------------------------------------------------------------------
void RLTModel::RemoveBet(const unsigned int& indexField, const unsigned int& value)
{
	this->UnsetBet(indexField, value);
}
//-------------------------------------------------------------------------------------------------------------
void RLTModel::CheckFallInHole()
{
    // ���� ����� �� ���� � �� ��������� � ��������� �������� ������� ������, �� ����� �� �������� ����� ��������
	if(!this->fallInHole && this->currentState == MODEL_STATE_WAIT_WARIK)
    {
        // ����� �� ���������� ����� � ����� �� ����
        printf("WARNING - ����� � ����� �� ����. �������� ����� ��������!!!!\n");
		this->UpdateState(MODEL_STATE_SHOW_WIN);
    }
}
//-------------------------------------------------------------------------------------------------------------
bool RLTModel::IsEnoughLimit(const unsigned int& field, const unsigned int& value) const
{
	const std::map<int, int> limits = fakeClient->GetLimits();
	const RLT_BetsOnZone& betsOnZOne = this->betsOnZone.at(field);
    
	// ����� ��������� ��� ������� ����
	const unsigned int limit = limits.at(BASE_COEFF_BET/betsOnZOne.coeff);
    // ��������, ������ ����� ���� �� ������� ������ �� ����
    const unsigned int willValue = betsOnZOne.bet + value;
    return limit >= willValue;
}
//-------------------------------------------------------------------------------------------------------------
bool RLTModel::IsEnoughCommonLimit(const unsigned int& value) const
{
    const std::map<int, int> limits = fakeClient->GetLimits();
    const unsigned int limit = limits.at(kTOTAL_LIMIT);
    return (this->totalBet + value) <= limit;
}
//-------------------------------------------------------------------------------------------------------------
void RLTModel::MakeBet(const unsigned int& indexField, const unsigned int& value)
{
	totalBet += value;
	RLT_BetsOnZone& zone = betsOnZone.at(indexField);
	zone.bet += value;
	for(int i = 0; i < zone.length; ++i)
    {
        const unsigned int currentBet = value * zone.coeff;
		const unsigned int number = zone.numbersMakeBets[i];
		this->betsToServer[number] += currentBet;
    }
}
//-------------------------------------------------------------------------------------------------------------
void RLTModel::UnsetBet(const unsigned int& indexField, const unsigned int& value)
{
	if(totalBet <= 0)
        return;
    
    totalBet -= value;
    RLT_BetsOnZone& zone = betsOnZone.at(indexField);
	if(zone.bet <= value)
        zone.bet = 0;
    else
        zone.bet -= value;
	for(int i = 0; i < zone.length; ++i)
    {
		const unsigned int currentBet = value * zone.coeff;
        const unsigned int number = zone.numbersMakeBets[i];
        if(betsToServer[number] <= currentBet)
            betsToServer[number] = 0;
        else
            betsToServer[number] -= currentBet;
    }
    
	ScriptingCore* core = ScriptingCore::getInstance();
	JSObject* object = JS_NewObject(core->getGlobalContext(), NULL, NULL, NULL); 
	JS_DefineProperty(core->getGlobalContext(), object, "indexField", INT_TO_JSVAL(indexField), NULL, NULL, JSPROP_READONLY);
	JS_DefineProperty(core->getGlobalContext(), object, "value", INT_TO_JSVAL(value), NULL, NULL, JSPROP_READONLY);

	jsval arg = OBJECT_TO_JSVAL(object);

	core->executeFunctionWithOwner(OBJECT_TO_JSVAL(viewDelegate), "didCancelBet", 1, &arg, NULL);				
}
//-------------------------------------------------------------------------------------------------------------
void RLTModel::CalculateWinningAndLossingFields()
{
	losingFields.clear();
	winningFields.clear();

	if(totalBet <= 0)
        return;
    
	const unsigned int winNumber = this->GetWinNumber();
    bool findNumberInZone = false;
    for(int i = 0; i < NUMBER_OF_FIELDS; ++i)
    {
        // ��������� ������ �� ����, ��� ���� ������� ������
        const RLT_BetsOnZone& currentBetsOnZone = betsOnZone.at(i);
        findNumberInZone = false;
        if(currentBetsOnZone.bet > 0)
        {
            for(int k = 0; k < currentBetsOnZone.length; ++k)
            {
                if(currentBetsOnZone.numbersMakeBets[k] == winNumber)
                {
					winningFields.push_back(i);
					findNumberInZone = true;
                    break;
                }
            }
            if(findNumberInZone == false)
                // ������ �� ����� ������ ����������� ����
                losingFields.push_back(i);
        }
    }
}
//-------------------------------------------------------------------------------------------------------------
	