#include "rlt_model_binding.hpp"
#include "cocos2d_specifics.hpp"
#include "RLT_Model.h"

template<class T>
static bool dummy_constructor(JSContext *cx, uint32_t argc, jsval *vp) {
    JS::RootedValue initializing(cx);
    bool isNewValid = true;
    if (isNewValid)
    {
        TypeTest<T> t;
        js_type_class_t *typeClass = nullptr;
        std::string typeName = t.s_name();
        auto typeMapIter = _js_global_type_map.find(typeName);
        CCASSERT(typeMapIter != _js_global_type_map.end(), "Can't find the class type!");
        typeClass = typeMapIter->second;
        CCASSERT(typeClass, "The value is null.");

        JSObject *_tmp = JS_NewObject(cx, typeClass->jsclass, typeClass->proto, typeClass->parentProto);
        T* cobj = new T();
        js_proxy_t *pp = jsb_new_proxy(cobj, _tmp);
        JS_AddObjectRoot(cx, &pp->obj);
        JS_SET_RVAL(cx, vp, OBJECT_TO_JSVAL(_tmp));
        return true;
    }

    return false;
}

static bool empty_constructor(JSContext *cx, uint32_t argc, jsval *vp) {
    return false;
}

static bool js_is_native_obj(JSContext *cx, JS::HandleObject obj, JS::HandleId id, JS::MutableHandleValue vp)
{
    vp.set(BOOLEAN_TO_JSVAL(true));
    return true;    
}
JSClass  *jsb_RLTModel_class;
JSObject *jsb_RLTModel_prototype;

bool js_rlt_model_binding_RLTModel_getString(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_getString : Invalid Native Object");
    if (argc == 0) {
        std::string ret = cobj->getString();
        jsval jsret = JSVAL_NULL;
        jsret = std_string_to_jsval(cx, ret);
        JS_SET_RVAL(cx, vp, jsret);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_getString : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_GetTotalBet(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_GetTotalBet : Invalid Native Object");
    if (argc == 0) {
        unsigned int ret = cobj->GetTotalBet();
        jsval jsret = JSVAL_NULL;
        jsret = uint32_to_jsval(cx, ret);
        JS_SET_RVAL(cx, vp, jsret);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_GetTotalBet : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_GetHistory(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_GetHistory : Invalid Native Object");
    if (argc == 0) {
        std::vector<int> ret = cobj->GetHistory();
        jsval jsret = JSVAL_NULL;
        jsret = std_vector_int_to_jsval(cx, ret);
        JS_SET_RVAL(cx, vp, jsret);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_GetHistory : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_DidFallInHole(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_DidFallInHole : Invalid Native Object");
    if (argc == 0) {
        cobj->DidFallInHole();
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_DidFallInHole : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_GetNominalsChip(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_GetNominalsChip : Invalid Native Object");
    if (argc == 0) {
        std::vector<int> ret = cobj->GetNominalsChip();
        jsval jsret = JSVAL_NULL;
        jsret = std_vector_int_to_jsval(cx, ret);
        JS_SET_RVAL(cx, vp, jsret);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_GetNominalsChip : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_HistoryObtained(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_HistoryObtained : Invalid Native Object");
    if (argc == 0) {
        cobj->HistoryObtained();
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_HistoryObtained : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_GetWin(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_GetWin : Invalid Native Object");
    if (argc == 0) {
        unsigned int ret = cobj->GetWin();
        jsval jsret = JSVAL_NULL;
        jsret = uint32_to_jsval(cx, ret);
        JS_SET_RVAL(cx, vp, jsret);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_GetWin : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_StopGame(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_StopGame : Invalid Native Object");
    if (argc == 0) {
        cobj->StopGame();
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_StopGame : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_GetWinningFields(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_GetWinningFields : Invalid Native Object");
    if (argc == 0) {
        std::vector<int> ret = cobj->GetWinningFields();
        jsval jsret = JSVAL_NULL;
        jsret = std_vector_int_to_jsval(cx, ret);
        JS_SET_RVAL(cx, vp, jsret);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_GetWinningFields : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_Update(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_Update : Invalid Native Object");
    if (argc == 0) {
        cobj->Update();
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_Update : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_BetSended(JSContext *cx, uint32_t argc, jsval *vp)
{
    jsval *argv = JS_ARGV(cx, vp);
    bool ok = true;
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_BetSended : Invalid Native Object");
    if (argc == 2) {
        bool arg0;
        std::string arg1;
        arg0 = JS::ToBoolean(JS::RootedValue(cx, argv[0]));
        ok &= jsval_to_std_string(cx, argv[1], &arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_rlt_model_binding_RLTModel_BetSended : Error processing arguments");
        cobj->BetSended(arg0, arg1);
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_BetSended : wrong number of arguments: %d, was expecting %d", argc, 2);
    return false;
}
bool js_rlt_model_binding_RLTModel_DisconnectWithError(JSContext *cx, uint32_t argc, jsval *vp)
{
    jsval *argv = JS_ARGV(cx, vp);
    bool ok = true;
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_DisconnectWithError : Invalid Native Object");
    if (argc == 1) {
        std::string arg0;
        ok &= jsval_to_std_string(cx, argv[0], &arg0);
        JSB_PRECONDITION2(ok, cx, false, "js_rlt_model_binding_RLTModel_DisconnectWithError : Error processing arguments");
        cobj->DisconnectWithError(arg0);
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_DisconnectWithError : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_rlt_model_binding_RLTModel_RestoreBets(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_RestoreBets : Invalid Native Object");
    if (argc == 0) {
        cobj->RestoreBets();
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_RestoreBets : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_InitField(JSContext *cx, uint32_t argc, jsval *vp)
{
    jsval *argv = JS_ARGV(cx, vp);
    bool ok = true;
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_InitField : Invalid Native Object");
    if (argc == 2) {
        unsigned int arg0;
        std::vector<int> arg1;
        ok &= jsval_to_uint32(cx, argv[0], &arg0);
        ok &= jsval_to_std_vector_int(cx, argv[1], &arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_rlt_model_binding_RLTModel_InitField : Error processing arguments");
        cobj->InitField(arg0, arg1);
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_InitField : wrong number of arguments: %d, was expecting %d", argc, 2);
    return false;
}
bool js_rlt_model_binding_RLTModel_RemoveBet(JSContext *cx, uint32_t argc, jsval *vp)
{
    jsval *argv = JS_ARGV(cx, vp);
    bool ok = true;
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_RemoveBet : Invalid Native Object");
    if (argc == 2) {
        unsigned int arg0;
        unsigned int arg1;
        ok &= jsval_to_uint32(cx, argv[0], &arg0);
        ok &= jsval_to_uint32(cx, argv[1], &arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_rlt_model_binding_RLTModel_RemoveBet : Error processing arguments");
        cobj->RemoveBet(arg0, arg1);
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_RemoveBet : wrong number of arguments: %d, was expecting %d", argc, 2);
    return false;
}
bool js_rlt_model_binding_RLTModel_GetCurrentState(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_GetCurrentState : Invalid Native Object");
    if (argc == 0) {
        int ret = (int)cobj->GetCurrentState();
        jsval jsret = JSVAL_NULL;
        jsret = int32_to_jsval(cx, ret);
        JS_SET_RVAL(cx, vp, jsret);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_GetCurrentState : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_StateObtained(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_StateObtained : Invalid Native Object");
    if (argc == 0) {
        cobj->StateObtained();
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_StateObtained : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_NominalsObtained(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_NominalsObtained : Invalid Native Object");
    if (argc == 0) {
        cobj->NominalsObtained();
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_NominalsObtained : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_GetCredits(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_GetCredits : Invalid Native Object");
    if (argc == 0) {
        unsigned int ret = cobj->GetCredits();
        jsval jsret = JSVAL_NULL;
        jsret = uint32_to_jsval(cx, ret);
        JS_SET_RVAL(cx, vp, jsret);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_GetCredits : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_SessionOpened(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_SessionOpened : Invalid Native Object");
    if (argc == 0) {
        cobj->SessionOpened();
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_SessionOpened : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_SetBet(JSContext *cx, uint32_t argc, jsval *vp)
{
    jsval *argv = JS_ARGV(cx, vp);
    bool ok = true;
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_SetBet : Invalid Native Object");
    if (argc == 2) {
        unsigned int arg0;
        unsigned int arg1;
        ok &= jsval_to_uint32(cx, argv[0], &arg0);
        ok &= jsval_to_uint32(cx, argv[1], &arg1);
        JSB_PRECONDITION2(ok, cx, false, "js_rlt_model_binding_RLTModel_SetBet : Error processing arguments");
        cobj->SetBet(arg0, arg1);
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_SetBet : wrong number of arguments: %d, was expecting %d", argc, 2);
    return false;
}
bool js_rlt_model_binding_RLTModel_DidEndShowWin(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_DidEndShowWin : Invalid Native Object");
    if (argc == 0) {
        cobj->DidEndShowWin();
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_DidEndShowWin : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_GetWinNumber(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_GetWinNumber : Invalid Native Object");
    if (argc == 0) {
        unsigned int ret = cobj->GetWinNumber();
        jsval jsret = JSVAL_NULL;
        jsret = uint32_to_jsval(cx, ret);
        JS_SET_RVAL(cx, vp, jsret);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_GetWinNumber : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_StartGame(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_StartGame : Invalid Native Object");
    if (argc == 0) {
        cobj->StartGame();
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_StartGame : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_GetLosingFields(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_GetLosingFields : Invalid Native Object");
    if (argc == 0) {
        std::vector<int> ret = cobj->GetLosingFields();
        jsval jsret = JSVAL_NULL;
        jsret = std_vector_int_to_jsval(cx, ret);
        JS_SET_RVAL(cx, vp, jsret);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_GetLosingFields : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_SetDelegate(JSContext *cx, uint32_t argc, jsval *vp)
{
    jsval *argv = JS_ARGV(cx, vp);
    bool ok = true;
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_SetDelegate : Invalid Native Object");
    if (argc == 1) {
        JSObject* arg0;
        do {
            if (!argv[0].isObject()) { ok = false; break; }
            js_proxy_t *jsProxy;
            JSObject *tmpObj = JSVAL_TO_OBJECT(argv[0]);
            jsProxy = jsb_get_js_proxy(tmpObj);
            arg0 = (JSObject*)(jsProxy ? jsProxy->ptr : NULL);
            JSB_PRECONDITION2( arg0, cx, false, "Invalid Native Object");
        } while (0);
        JSB_PRECONDITION2(ok, cx, false, "js_rlt_model_binding_RLTModel_SetDelegate : Error processing arguments");
        cobj->SetDelegate(arg0);
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_SetDelegate : wrong number of arguments: %d, was expecting %d", argc, 1);
    return false;
}
bool js_rlt_model_binding_RLTModel_LimitsObtained(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_LimitsObtained : Invalid Native Object");
    if (argc == 0) {
        cobj->LimitsObtained();
        JS_SET_RVAL(cx, vp, JSVAL_VOID);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_LimitsObtained : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_GetRemainedTimeState(JSContext *cx, uint32_t argc, jsval *vp)
{
    JSObject *obj = JS_THIS_OBJECT(cx, vp);
    js_proxy_t *proxy = jsb_get_js_proxy(obj);
    RLTModel* cobj = (RLTModel *)(proxy ? proxy->ptr : NULL);
    JSB_PRECONDITION2( cobj, cx, false, "js_rlt_model_binding_RLTModel_GetRemainedTimeState : Invalid Native Object");
    if (argc == 0) {
        double ret = cobj->GetRemainedTimeState();
        jsval jsret = JSVAL_NULL;
        jsret = DOUBLE_TO_JSVAL(ret);
        JS_SET_RVAL(cx, vp, jsret);
        return true;
    }

    JS_ReportError(cx, "js_rlt_model_binding_RLTModel_GetRemainedTimeState : wrong number of arguments: %d, was expecting %d", argc, 0);
    return false;
}
bool js_rlt_model_binding_RLTModel_constructor(JSContext *cx, uint32_t argc, jsval *vp)
{
    jsval *argv = JS_ARGV(cx, vp);
    bool ok = true;
    RLTModel* cobj = new (std::nothrow) RLTModel();
    TypeTest<RLTModel> t;
    js_type_class_t *typeClass = nullptr;
    std::string typeName = t.s_name();
    auto typeMapIter = _js_global_type_map.find(typeName);
    CCASSERT(typeMapIter != _js_global_type_map.end(), "Can't find the class type!");
    typeClass = typeMapIter->second;
    CCASSERT(typeClass, "The value is null.");
    JSObject *obj = JS_NewObject(cx, typeClass->jsclass, typeClass->proto, typeClass->parentProto);
    JS_SET_RVAL(cx, vp, OBJECT_TO_JSVAL(obj));
    // link the native object with the javascript object
    js_proxy_t* p = jsb_new_proxy(cobj, obj);
    if (JS_HasProperty(cx, obj, "_ctor", &ok) && ok)
        ScriptingCore::getInstance()->executeFunctionWithOwner(OBJECT_TO_JSVAL(obj), "_ctor", argc, argv);
    return true;
}



void js_RLTModel_finalize(JSFreeOp *fop, JSObject *obj) {
    CCLOGINFO("jsbindings: finalizing JS object %p (RLTModel)", obj);
    js_proxy_t* nproxy;
    js_proxy_t* jsproxy;
    jsproxy = jsb_get_js_proxy(obj);
    if (jsproxy) {
        nproxy = jsb_get_native_proxy(jsproxy->ptr);

        RLTModel *nobj = static_cast<RLTModel *>(nproxy->ptr);
        if (nobj)
            delete nobj;
        
        jsb_remove_proxy(nproxy, jsproxy);
    }
}

void js_register_rlt_model_binding_RLTModel(JSContext *cx, JSObject *global) {
    jsb_RLTModel_class = (JSClass *)calloc(1, sizeof(JSClass));
    jsb_RLTModel_class->name = "RLTModel";
    jsb_RLTModel_class->addProperty = JS_PropertyStub;
    jsb_RLTModel_class->delProperty = JS_DeletePropertyStub;
    jsb_RLTModel_class->getProperty = JS_PropertyStub;
    jsb_RLTModel_class->setProperty = JS_StrictPropertyStub;
    jsb_RLTModel_class->enumerate = JS_EnumerateStub;
    jsb_RLTModel_class->resolve = JS_ResolveStub;
    jsb_RLTModel_class->convert = JS_ConvertStub;
    jsb_RLTModel_class->finalize = js_RLTModel_finalize;
    jsb_RLTModel_class->flags = JSCLASS_HAS_RESERVED_SLOTS(2);

    static JSPropertySpec properties[] = {
        {"__nativeObj", 0, JSPROP_ENUMERATE | JSPROP_PERMANENT, JSOP_WRAPPER(js_is_native_obj), JSOP_NULLWRAPPER},
        {0, 0, 0, JSOP_NULLWRAPPER, JSOP_NULLWRAPPER}
    };

    static JSFunctionSpec funcs[] = {
        JS_FN("getString", js_rlt_model_binding_RLTModel_getString, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("GetTotalBet", js_rlt_model_binding_RLTModel_GetTotalBet, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("GetHistory", js_rlt_model_binding_RLTModel_GetHistory, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("DidFallInHole", js_rlt_model_binding_RLTModel_DidFallInHole, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("GetNominalsChip", js_rlt_model_binding_RLTModel_GetNominalsChip, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("HistoryObtained", js_rlt_model_binding_RLTModel_HistoryObtained, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("GetWin", js_rlt_model_binding_RLTModel_GetWin, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("StopGame", js_rlt_model_binding_RLTModel_StopGame, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("GetWinningFields", js_rlt_model_binding_RLTModel_GetWinningFields, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("Update", js_rlt_model_binding_RLTModel_Update, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("BetSended", js_rlt_model_binding_RLTModel_BetSended, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("DisconnectWithError", js_rlt_model_binding_RLTModel_DisconnectWithError, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("RestoreBets", js_rlt_model_binding_RLTModel_RestoreBets, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("InitField", js_rlt_model_binding_RLTModel_InitField, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("RemoveBet", js_rlt_model_binding_RLTModel_RemoveBet, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("GetCurrentState", js_rlt_model_binding_RLTModel_GetCurrentState, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("StateObtained", js_rlt_model_binding_RLTModel_StateObtained, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("NominalsObtained", js_rlt_model_binding_RLTModel_NominalsObtained, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("GetCredits", js_rlt_model_binding_RLTModel_GetCredits, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("SessionOpened", js_rlt_model_binding_RLTModel_SessionOpened, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("SetBet", js_rlt_model_binding_RLTModel_SetBet, 2, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("DidEndShowWin", js_rlt_model_binding_RLTModel_DidEndShowWin, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("GetWinNumber", js_rlt_model_binding_RLTModel_GetWinNumber, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("StartGame", js_rlt_model_binding_RLTModel_StartGame, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("GetLosingFields", js_rlt_model_binding_RLTModel_GetLosingFields, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("SetDelegate", js_rlt_model_binding_RLTModel_SetDelegate, 1, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("LimitsObtained", js_rlt_model_binding_RLTModel_LimitsObtained, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FN("GetRemainedTimeState", js_rlt_model_binding_RLTModel_GetRemainedTimeState, 0, JSPROP_PERMANENT | JSPROP_ENUMERATE),
        JS_FS_END
    };

    JSFunctionSpec *st_funcs = NULL;

    jsb_RLTModel_prototype = JS_InitClass(
        cx, global,
        NULL, // parent proto
        jsb_RLTModel_class,
        js_rlt_model_binding_RLTModel_constructor, 0, // constructor
        properties,
        funcs,
        NULL, // no static properties
        st_funcs);
    // make the class enumerable in the registered namespace
//  bool found;
//FIXME: Removed in Firefox v27 
//  JS_SetPropertyAttributes(cx, global, "RLTModel", JSPROP_ENUMERATE | JSPROP_READONLY, &found);

    // add the proto and JSClass to the type->js info hash table
    TypeTest<RLTModel> t;
    js_type_class_t *p;
    std::string typeName = t.s_name();
    if (_js_global_type_map.find(typeName) == _js_global_type_map.end())
    {
        p = (js_type_class_t *)malloc(sizeof(js_type_class_t));
        p->jsclass = jsb_RLTModel_class;
        p->proto = jsb_RLTModel_prototype;
        p->parentProto = NULL;
        _js_global_type_map.insert(std::make_pair(typeName, p));
    }
}

void register_all_rlt_model_binding(JSContext* cx, JSObject* obj) {

    js_register_rlt_model_binding_RLTModel(cx, obj);
}

