#ifndef __rlt_model_binding_h__
#define __rlt_model_binding_h__

#include "jsapi.h"
#include "jsfriendapi.h"


extern JSClass  *jsb_RLTModel_class;
extern JSObject *jsb_RLTModel_prototype;

bool js_rlt_model_binding_RLTModel_constructor(JSContext *cx, uint32_t argc, jsval *vp);
void js_rlt_model_binding_RLTModel_finalize(JSContext *cx, JSObject *obj);
void js_register_rlt_model_binding_RLTModel(JSContext *cx, JSObject *global);
void register_all_rlt_model_binding(JSContext* cx, JSObject* obj);
bool js_rlt_model_binding_RLTModel_getString(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_GetTotalBet(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_GetHistory(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_DidFallInHole(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_GetNominalsChip(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_HistoryObtained(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_GetWin(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_StopGame(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_GetWinningFields(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_Update(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_BetSended(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_DisconnectWithError(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_RestoreBets(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_InitField(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_RemoveBet(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_GetCurrentState(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_StateObtained(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_NominalsObtained(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_GetCredits(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_SessionOpened(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_SetBet(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_DidEndShowWin(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_GetWinNumber(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_StartGame(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_GetLosingFields(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_SetDelegate(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_LimitsObtained(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_GetRemainedTimeState(JSContext *cx, uint32_t argc, jsval *vp);
bool js_rlt_model_binding_RLTModel_RLTModel(JSContext *cx, uint32_t argc, jsval *vp);
#endif

