
/**
 * @class RLTModel
 */
RLTModel = {

/**
 * @method getString
 * @return {String}
 */
getString : function (
)
{
    return ;
},

/**
 * @method GetTotalBet
 * @return {unsigned int}
 */
GetTotalBet : function (
)
{
    return 0;
},

/**
 * @method GetHistory
 * @return {Array}
 */
GetHistory : function (
)
{
    return new Array();
},

/**
 * @method DidFallInHole
 */
DidFallInHole : function (
)
{
},

/**
 * @method GetNominalsChip
 * @return {Array}
 */
GetNominalsChip : function (
)
{
    return new Array();
},

/**
 * @method HistoryObtained
 */
HistoryObtained : function (
)
{
},

/**
 * @method GetWin
 * @return {unsigned int}
 */
GetWin : function (
)
{
    return 0;
},

/**
 * @method StopGame
 */
StopGame : function (
)
{
},

/**
 * @method GetWinningFields
 * @return {Array}
 */
GetWinningFields : function (
)
{
    return new Array();
},

/**
 * @method Update
 */
Update : function (
)
{
},

/**
 * @method BetSended
 * @param {bool} arg0
 * @param {String} arg1
 */
BetSended : function (
bool, 
str 
)
{
},

/**
 * @method DisconnectWithError
 * @param {String} arg0
 */
DisconnectWithError : function (
str 
)
{
},

/**
 * @method RestoreBets
 */
RestoreBets : function (
)
{
},

/**
 * @method InitField
 * @param {unsigned int} arg0
 * @param {Array} arg1
 */
InitField : function (
int, 
array 
)
{
},

/**
 * @method RemoveBet
 * @param {unsigned int} arg0
 * @param {unsigned int} arg1
 */
RemoveBet : function (
int, 
int 
)
{
},

/**
 * @method GetCurrentState
 * @return {RLTModel::RLTMODELSTATE}
 */
GetCurrentState : function (
)
{
    return RLTModel::RLTMODELSTATE;
},

/**
 * @method StateObtained
 */
StateObtained : function (
)
{
},

/**
 * @method NominalsObtained
 */
NominalsObtained : function (
)
{
},

/**
 * @method GetCredits
 * @return {unsigned int}
 */
GetCredits : function (
)
{
    return 0;
},

/**
 * @method SessionOpened
 */
SessionOpened : function (
)
{
},

/**
 * @method SetBet
 * @param {unsigned int} arg0
 * @param {unsigned int} arg1
 */
SetBet : function (
int, 
int 
)
{
},

/**
 * @method DidEndShowWin
 */
DidEndShowWin : function (
)
{
},

/**
 * @method GetWinNumber
 * @return {unsigned int}
 */
GetWinNumber : function (
)
{
    return 0;
},

/**
 * @method StartGame
 */
StartGame : function (
)
{
},

/**
 * @method GetLosingFields
 * @return {Array}
 */
GetLosingFields : function (
)
{
    return new Array();
},

/**
 * @method SetDelegate
 * @param {JSObject} arg0
 */
SetDelegate : function (
jsobject 
)
{
},

/**
 * @method LimitsObtained
 */
LimitsObtained : function (
)
{
},

/**
 * @method GetRemainedTimeState
 * @return {float}
 */
GetRemainedTimeState : function (
)
{
    return 0;
},

/**
 * @method RLTModel
 * @constructor
 */
RLTModel : function (
)
{
},

};
