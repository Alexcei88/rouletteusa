#include "SHTimer.h"
#include <sys\timeb.h>

//------------------------------------------------------------------------------------	
SHTimer::SHTimer(void) :
	 callback(nullptr)
	,startTime(-1)
{}
//------------------------------------------------------------------------------------	
SHTimer::~SHTimer(void)
{}
//------------------------------------------------------------------------------------	
void SHTimer::Update()
{	
	if(callback)
	{
		if(GetTime() - startTime > periodTime)
		{
			callback();
		}
	}
}	
//------------------------------------------------------------------------------------	
void SHTimer::SetTimer(const unsigned long long& periodTime, std::function<void ()> callback)
{
	startTime = GetTime();
	this->periodTime = periodTime;
	this->callback = callback;
}
//------------------------------------------------------------------------------------	
void SHTimer::ResetTimer()
{
	callback = nullptr;
	periodTime = 0;
	startTime = -1;
}
//------------------------------------------------------------------------------------	
unsigned long long SHTimer::GetTime()
{
	struct timeb lt;
	ftime(&lt);
	return lt.time;
}
//------------------------------------------------------------------------------------	
unsigned long long SHTimer::GetPeriodTime(const unsigned long long& startTime)
{
	return GetTime() - startTime;
}
//------------------------------------------------------------------------------------	


