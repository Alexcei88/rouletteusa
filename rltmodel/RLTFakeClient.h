#pragma once

#include <map>
#include <vector>

class RLTModel;

class RLTFakeClient
{
public:
	RLTFakeClient(RLTModel* delegate);
	~RLTFakeClient();

	enum RLTCLIENTSTATE
	{
		STATE_UNKNOWN        = 1000000,
		STATE_MAKE_YOUR_BETS = 1,
		STATE_NO_MORE_BETS   = 2,
		STATE_SHOW_WIN       = 3
	};

	/** ������� */
	unsigned int GetCredits() { return credits; }
	/** ���������� ����� */
	unsigned int GetWinNumber() { return winNumber; }
	/** ������� � �������� */
	unsigned int GetWinInCredits() { return winInCredits; }
	/** ��������� ����� ����� ������ ��������� */
	float GetRemainedTime() { return remainedTime; }

	std::vector<int> GetHistory() { return history; }
	/** ������ ������ */
	std::map<int, int> GetLimits() { return limits; }
    /** �������� ����� */
	std::vector<int> GetNominals() { return nominals; }
	/** ��������� ������� */
	RLTCLIENTSTATE GetState() { return state; }
	/** ����� �������������� ����� */
	unsigned int GetNumberSpin() { return numberSpin; }
	
	
	/** @brief ��������� �������� ������� */
	void GetConfig();
	/** @brief ��������� ������� */
	void UpdateHistory();
	/** @brief ��������� ������ ��������� �� ������� */
	void UpdateState();
	/** @brief �������� ������ �� ������ */
	void SendBets(const std::vector<int>& bets);
	
private:
	/** ������� */
	unsigned int credits;
	/** ���������� ����� */
	unsigned int winNumber;
	/** ������� � �������� */
	unsigned int winInCredits;
	/** ��������� ����� ����� ������ ��������� */
	float remainedTime;
	/** ��������� ������� */
	RLTCLIENTSTATE state;
	/** ������ ������ */
	std::map<int, int> limits;
    /** �������� ����� */
	std::vector<int> nominals;
    /** ������� �������� ������� */
	std::vector<int> history;
    /** ������ */
	std::vector<int> bets;
    /** ����� �������������� ����� */
	unsigned int numberSpin;
	
	RLTModel* delegate;

	void ShellGetConfig();
	void ShellGetHistory();
	void ShellGetState();
};