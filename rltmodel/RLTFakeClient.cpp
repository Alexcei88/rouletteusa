//#include "stdafx.h"
#include "RLTFakeClient.h"
#include "RLT_Model.h"

using namespace std;

//------------------------------------------------------------------------------------
RLTFakeClient::RLTFakeClient(RLTModel* delegate)
{
	this->delegate = delegate;
	int history [] = {10, 20, 30, 4, 6, 1, 8, 10, 2, 0, 11, 12, 13, 14, 15};
	this->history.assign(&history[0], &history[15]);
	this->state = STATE_UNKNOWN;
	numberSpin = 0;
}
//------------------------------------------------------------------------------------
RLTFakeClient::~RLTFakeClient()
{
}
//-------------------------------------------------------------------------------------------------------------
void RLTFakeClient::GetConfig()
{
	this->ShellGetConfig();
}
//-------------------------------------------------------------------------------------------------------------
void RLTFakeClient::UpdateHistory()
{
	this->ShellGetHistory();
}
//-------------------------------------------------------------------------------------------------------------
void RLTFakeClient::UpdateState()
{
	this->ShellGetState();
}
//-------------------------------------------------------------------------------------------------------------
void RLTFakeClient::SendBets(const vector<int>& bets)
{
	this->bets.assign(bets.begin(), bets.end());
	delegate->BetSended(true, "");
}
//------------------------------------------------------------------------------------
void RLTFakeClient::ShellGetConfig()
{
	credits = 20000;

	limits[1] = 500;
	limits[2] = 1000;
	limits[3] = 1500;
	limits[4] = 2000;
	limits[5] = 2500;
	limits[6] = 3000;
	limits[12] = 4000;
	limits[18] = 5000;
	limits[36] = 10000;

	int nominals [] = {1, 5, 10, 50, 100, 500, 1000};
	this->nominals.assign(&nominals[0], &nominals[7]);

	delegate->LimitsObtained();
	delegate->NominalsObtained();
}
//------------------------------------------------------------------------------------
void RLTFakeClient::ShellGetHistory()
{
	delegate->HistoryObtained();
}
//------------------------------------------------------------------------------------
void RLTFakeClient::ShellGetState()
{
	vector<int> historyTemp(this->history);
	switch (state) 
	{
        case STATE_SHOW_WIN:
            remainedTime = 30.0f;
            state = STATE_MAKE_YOUR_BETS;
            ++numberSpin;
            break;

        case STATE_NO_MORE_BETS:
		{
            remainedTime = 10.0f;
            state = STATE_SHOW_WIN;
            
			winNumber = rand() % 37;
			winInCredits = this->bets[winNumber];
			
			unsigned int totalBet = 0;
			for(unsigned int i = 0; i < this->bets.size(); ++i)
            {
                totalBet += bets[i];
            }
			int credits = this->credits - totalBet/36;

            if (credits < 0)
            {
                credits = 0;
            }
            credits += winInCredits;
            this->credits += credits;
            
			printf("Total bet: %d\n", totalBet/36);
            printf("Win: %d\n", winInCredits);
                            
            // ������ ������ �������
            for(int i = 0; i < 14; ++i)
            {
                historyTemp[i + 1] = history[i];
            }
            historyTemp[0] = winNumber;
			this->history.assign(historyTemp.begin(), historyTemp.end());
            break;
		}
        case STATE_MAKE_YOUR_BETS:
            remainedTime = 5.0f;
            state = STATE_NO_MORE_BETS;
            break;

        case STATE_UNKNOWN:
            remainedTime = 30.0f;
            state = STATE_MAKE_YOUR_BETS;
            break;
            
        default:
            break;
    }
	delegate->StateObtained();
}
//------------------------------------------------------------------------------------

